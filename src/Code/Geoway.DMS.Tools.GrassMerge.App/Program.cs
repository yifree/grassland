﻿using Geoway.DMS.Tools.GrassMerge.App.Properties;
using Geoway.DMS.UI.GrassLand;
using Geoway.GDC.DMS.UI;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Geoway.DMS.Tools.GrassMerge.App
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AppRoot.Init1_Base();
            if (!AppRoot.Init3_Database(null))
            {
                return;
            }

            var mainForm = new FormMain();
            mainForm.Init(AppRoot.DB);
            Application.Run(mainForm);
        }
    }
}
