﻿using DevExpress.XtraEditors;
using Geoway.ADF.Core.Config;
using Geoway.ADF.Core.Controls;
using Geoway.ADF.Core.DB;
using Geoway.ADF.Core.Utility;
using Geoway.ADF.GDC.Dataset;
using Geoway.ADF.GDC.GeowayDataset;
using Geoway.DMS.UI.GrassLand;
using Geoway.GDC.DMS.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geoway.DMS.Tools.GrassMerge.App
{
    public partial class FormMain : XtraForm
    {
        private IDBHelper _db;
        const string TXT_PAUSE = "暂停";
        const string TXT_RESUME = "继续";
        private int DistrictCount = 0;
        private GrassMergeWorker backgroundWorker;
        private IDBHelper _spatialConn;

        public GwLogProgress Log { get; }

        public FormMain()
        {
            InitializeComponent();
            Log = new GwLogProgress()
            {
                Dock = DockStyle.Fill,
                TotalProgressVisible = false,
                RefreshButtonVisible = false
            };
            btnPause.Text = TXT_PAUSE;
            btnPause.Enabled = false;
            btnStop.Enabled = false;
            gpnlLog.Controls.Add(Log);
            StartPosition = FormStartPosition.CenterScreen;
            FormClosing += FormMain_FormClosing;
            Text = ConfigUtil.GetConfigValue("app_name", Text);
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker != null && this.backgroundWorker.ActiveWokerCount > 0)
            {
                DevMsg.Error("有正在执行的处理，请停止后退出");
                e.Cancel = true;
            }
            else
            {
                if (DevMsg.YesNo($"是否退出 {Text}?") != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        public void Init(IDBHelper db)
        {
            _db = db;
            var objectID = ObjectUsageHelper.GetUniqueUsageObject(AppRoot.DB, "MainGeoDatabase");
            var dbMgr = new GeoDatabaseManage();
            var spatilDb = dbMgr.GetDatabse(AppRoot.DB, objectID) as IGeowayWorkspace;
            var datasource = (spatilDb as Geoway.ADF.GDC.Dataset.GwGeoDatabase);
            _spatialConn = DBHelper.Load(datasource.ConnectionProperties);
            if (_spatialConn == null)
            {
                DevMsg.Error("未设置主空间数据源");
            }
        }

        Dictionary<string, string> LoadFieldMapping(out string path)
        {
            var dic = new Dictionary<string, string>();
            path = Path.Combine(Application.StartupPath, "grass_merge_fields.txt");
            try
            {
                if (File.Exists(path))
                {
                    var lines = File.ReadLines(path, Encoding.UTF8);
                    foreach (var line in lines)
                    {
                        var newline = System.Text.RegularExpressions.Regex.Replace(line.Trim(), @"\s+", " ");
                        if (newline.StartsWith("#") || !newline.Contains(" "))
                            continue;
                        var arr = newline.Split(' ');
                        if (!dic.ContainsKey(arr[0]))
                            dic.Add(arr[0], arr[1]);
                    }
                }
                else
                {
                    File.WriteAllText(path, "#字段1 字段2", Encoding.UTF8);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
            return dic;
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            Log.ClearLog();
            Log.MessagePanelVisible = false;
            var dict = LoadFieldMapping(out string configFile);
            if (dict.Count == 0)
            {
                DevMsg.Error("未设置更新字段");
                Process.Start("notepad.exe", configFile);
                return;
            }
            GrassMergeWorker.WorkerArgs args = GetWorkerArgs();
            if (args.DistrictList.Count == 0)
            {
                Log.AppendLine("无待处理的数据");
                return;
            }
            args.FieldMapping = dict;

            btnStart.Enabled = false;
            btnPause.Enabled = true;
            btnStop.Enabled = true;
            DistrictCount = args.DistrictList.Count;
            Log.SetCurrentProgressMax(DistrictCount);
            Log.SetCurrentProgressPosition(0);
            Log.AppendLine("正在启动任务...");
            backgroundWorker = new GrassMergeWorker();
            backgroundWorker.OnMessage += BackgroundWorker_OnMessage;
            backgroundWorker.OnProgressLeft += BackgroundWorker_OnProgressLeft;
            backgroundWorker.OnComplete += BackgroundWorker_OnComplete;
            backgroundWorker.Start(args);
            Log.AppendLine($"已启动{backgroundWorker.ActiveWokerCount}个任务...");
        }

        private void BackgroundWorker_OnComplete(bool success)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<bool>(BackgroundWorker_OnComplete), success);
            }
            else
            {
                if (backgroundWorker.ActiveWokerCount == 0)
                {
                    btnPause.Text = TXT_PAUSE;
                    btnStart.Enabled = true;
                    btnStop.Enabled = false;
                    btnPause.Enabled = false;
                }
            }
        }

        private void BackgroundWorker_OnProgressLeft(int pos)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<int>(BackgroundWorker_OnProgressLeft), pos);
            }
            else
            {
                Log.SetCurrentProgressPosition(DistrictCount - pos);
            }
        }

        private void BackgroundWorker_OnMessage(string msg)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(BackgroundWorker_OnMessage), msg);
            }
            else
            {
                Log.AppendLine(msg);
            }
        }

        private void BtnPause_Click(object sender, EventArgs e)
        {
            if (btnPause.Text == TXT_PAUSE)
            {
                Log.AppendLine("正在恢复任务...");
                backgroundWorker.Continue();
                Log.AppendLine("任务已经恢复...");
                btnPause.Text = TXT_RESUME;
            }
            else
            {
                Log.AppendLine("正在暂停任务...");
                backgroundWorker.Pause();
                Log.AppendLine("任务已经暂停...");
                btnPause.Text = TXT_PAUSE;
            }
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            Log.AppendLine("正在停止任务...");
            btnPause.Text = TXT_PAUSE;
            backgroundWorker.Stop();
            btnPause.Enabled = false;
            btnStop.Enabled = false;
        }

        GrassMergeWorker.WorkerArgs GetWorkerArgs()
        {
            var args = new GrassMergeWorker.WorkerArgs();
            args.DB = _spatialConn;
            args.DistrictList.Clear();
            var sql = $"SELECT code dm FROM tb_xzq_dltb where status=0";
            var reader = _spatialConn.DoQuery(sql);
            while (reader.Read())
            {
                args.DistrictList.Push(new ADF.GDC.Catalog.GwDistrictItem() { Code = reader[0].ToString() });
            }
            reader.Close();
            return args;
        }

        private void BtnMapping_Click(object sender, EventArgs e)
        {
            LoadFieldMapping(out string path);
            Process.Start("notepad.exe", path);
        }
    }
}
