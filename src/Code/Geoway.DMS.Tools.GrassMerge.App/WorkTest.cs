﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace Geoway.DMS.Tools.GrassMerge.App
{
    class WorkTest
    {
        private ManualResetEvent restSingal = new ManualResetEvent(true);
        private readonly List<BackgroundWorker> workerList = new List<BackgroundWorker>();

        public WorkTest()
        {

        }

        public void start()
        {
            for (int i = 0; i < 2; i++)
            {
                var wk = new BackgroundWorker();
                wk.DoWork += Wk_DoWork;
                workerList.Add(wk);
                wk.RunWorkerAsync();
            }
        }

        public void pause()
        {
            restSingal.Reset();
        }

        public void resume()
        {
            restSingal.Set();
        }

        private void Wk_DoWork(object sender, DoWorkEventArgs e)
        {
            var wk = sender as BackgroundWorker;

            while (restSingal.WaitOne())
            {
                Thread.Sleep(500);
                System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.ManagedThreadId);

            }

            //System.Diagnostics.Debug.WriteLine();
        }
    }
}
