--在空间库执行
DO $$
DECLARE
	v_row record;
v_cnt int;
v_sql text;
BEGIN
	create table if not EXISTS tb_xzq_dltb(code text,status int DEFAULT 0);
	FOR v_row IN SELECT DISTINCT
	substr( ZLDWDM, 1, 6 ) val 
	FROM
		dltb
		LOOP
	v_sql:='select count(1) from tb_xzq_dltb where code =$1';
		EXECUTE v_sql into v_cnt  USING v_row.val;		
	if(v_cnt = 0) then
			EXECUTE	'INSERT into tb_xzq_dltb (code,status) values($1,0)' USING v_row.val;
		end if;		
	end LOOP;
	raise notice '处理完成';
END $$;
