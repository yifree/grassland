﻿using Geoway.ADF.Core.DB;
using Geoway.ADF.Core.Utility;
using Geoway.ADF.GDC.Catalog;
using Geoway.DMS.UI.GrassLand;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;

namespace Geoway.DMS.Tools.GrassMerge.App
{
    class GrassMergeWorker
    {
        private ManualResetEvent restSingal = new ManualResetEvent(true);
        private readonly List<BackgroundWorker> workerList = new List<BackgroundWorker>();

        public int WokerCount { get; set; } = ConfigUtil.GetConfigValueInt32("threads", 1);

        public int ActiveWokerCount { get => workerList.Count; }

        public event Action<string> OnMessage;
        public event Action<int> OnProgressLeft;
        public event Action<bool> OnComplete;

        public GrassMergeWorker()
        {

        }

        private void GrassMergeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var args = e.Argument as WorkerArgs;
            var db = args.DB.Clone() as IDBHelper;
            while (restSingal.WaitOne())
            {
                var success = args.DistrictList.TryPop(out GwDistrictItem item);
                try
                {
                    if (success)
                    {
                        success = DoMerge(args, db, item);
                        if (success)
                        {
                            db.DoSQL($"update tb_xzq_dltb set status=1 where code='{item.Code}'");
                        }
                        else
                        {
                            OnMessage($"{item.Code} 处理失败");
                            e.Cancel = true;
                            db.DisConnect();
                            workerList.Remove(worker);
                            OnComplete?.Invoke(false);
                            CompleteWork(db, worker, false);
                            return;
                        }
                    }
                    else
                    {
                        CompleteWork(db, worker, true);
                        e.Cancel = true;
                        return;
                    }
                }
                finally
                {
                    OnProgressLeft?.Invoke(args.DistrictList.Count);
                }
                if (worker.CancellationPending)
                {
                    CompleteWork(db, worker, true);
                    return;
                }
            }
        }

        private void CompleteWork(IDBHelper db, BackgroundWorker worker, bool success)
        {
            db.DisConnect();
            workerList.Remove(worker);
            OnComplete?.Invoke(success);
        }

        internal void Start(WorkerArgs args)
        {
            for (int i = 0; i < WokerCount; i++)
            {
                var woker = new BackgroundWorker();
                woker.DoWork += GrassMergeWorker_DoWork;
                workerList.Add(woker);
                woker.RunWorkerAsync(args);
            }

        }

        public void Pause()
        {
            restSingal.Reset();
        }

        public void Stop()
        {
            foreach (var woker in this.workerList)
            {
                woker.CancelAsync();
            }
            restSingal.Set();
        }

        public void Continue()
        {
            restSingal.Set();
        }


        private bool DoMerge(WorkerArgs args, IDBHelper db, GwDistrictItem dist)
        {
            try
            {
                var fldMapping = string.Join(",", args.FieldMapping.Select(r => $"\"{r.Key}\"=t2.\"{r.Value}\""));
                string sql = $@"
UPDATE {SampleSetting.TB_DLTB} t0
    set {fldMapping}
FROM
    {SampleSetting.TB_DLTB} AS t1
    INNER JOIN {SampleSetting.TB_GRASS} AS t2 ON st_intersects(t1.shape, t2.shape)
WHERE
        t0.objectid = t1.objectid
    AND t1.{SampleSetting.FLD_DLBM} LIKE'04%'
    AND t1.{SampleSetting.FLD_XZQ} like '{dist.Code}%'
    AND st_area(st_intersection (t1.shape, t2.shape) ) / st_area(t1.shape) >= 0.7";
                var cnt = db.DoSQL(sql);
                OnMessage($"行政区 {dist.Code} 更新 {cnt} 个图斑");
                return true;
            }
            catch (Exception ex)
            {
                OnMessage($"行政区 {dist.Code} 更新出错：{ex.Message}");
                LogHelper.Error.Append(ex);
                return false;
            }
        }

        public class WorkerArgs
        {
            public IDBHelper DB { get; set; }

            public System.Collections.Concurrent.ConcurrentStack<GwDistrictItem> DistrictList { get; private set; } = new System.Collections.Concurrent.ConcurrentStack<GwDistrictItem>();

            public Dictionary<string, string> FieldMapping { get; set; } = new Dictionary<string, string>();
        }
    }
}
