﻿using DevExpress.Skins;
using DevExpress.UserSkins;
using Geoway.ADF.Core.Controls;
using Geoway.ADF.Core.Utility;
using Geoway.ADF.MIS.Business;
using Geoway.GDC.DMS.UI;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using Geoway.DMS.UI;
using Geoway.GDC.DMS.WPFBase;
using Geoway.ADF.Core.Config;
using Geoway.ADF.GDC.Dataset;
using Geoway.ADF.GDC.GeowayDataset;
using Geoway.ADF.Core.DB;
using Geoway.ADF.GIS.GeowayUtility;

namespace Geoway.ADF.AppForGeowayGDC
{
    static class Program
    {
        //在本应用程序内的唯一全局静态对象
        private static FormSplashBase frmSplash = null;
        private static Form frmMain = null; //系统窗体       
        private static int iSubsysMark = -1;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ApplicationExit += new EventHandler(OnApplicationExit);
            Application.ThreadException += Application_ThreadException;

            BonusSkins.Register();
            DevExpress.Utils.AppearanceObject.DefaultFont = new Font("微软雅黑", 9);
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Office 2013");
            SkinManager.EnableFormSkins();

            DevXLocalizer.DevXLocalize();

            //DMS.UI.GrassLand.QuadTree.QuardTreeCreator.Test();

            //string systemName = GetOperatorSystem();
            //if (!systemName.Contains("XP") && !IsRunAsAdmin())
            //{
            //    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //    //设置运行文件 
            //    startInfo.FileName = System.Windows.Forms.Application.ExecutablePath;
            //    //设置启动参数 
            //    startInfo.Arguments = String.Join(" ", args);
            //    //设置启动动作,确保以管理员身份运行 
            //    startInfo.Verb = "runas";
            //    //如果不是管理员，则启动UAC 
            //    System.Diagnostics.Process.Start(startInfo);
            //    Application.Exit();
            //}
            //else
            //{
            SystemArgs sysArgs = new SystemArgs(args);
            iSubsysMark = 20701;
            StartMainUI(iSubsysMark, sysArgs);
            OnApplicationExit(null, null);
            //}
        }

        private static void StartMainUI(int iSubSysMark, SystemArgs sysArgs)
        {
            try
            {
                //设置系统的标识号码、登录权限编号
                AppRoot.SubSysMark = iSubSysMark;
                AppRoot.SysLoginRightCode = RightCodeDefinition.Client_Login;

                //初始化Splash窗口
                ShowSplash();

                //设置图层加载信息的回调
                AppRoot.SetMsgCallBack(frmSplash);

                //if (CheckOutLicense() == false)
                //{
                //    return;
                //}

                //系统基本配置初始化
                AppRoot.Init1_Base();

                if (!AppRoot.Init2_GIS())
                {
                    return;
                }

                //初始化数据库连接，如果没有，则建立　
                if (!AppRoot.Init3_Database(sysArgs.DbConnString))
                {
                    return;
                }

#if DEBUG2
                var objectID = ObjectUsageHelper.GetUniqueUsageObject(AppRoot.DB, "MainGeoDatabase");
                var dbMgr = new GeoDatabaseManage();
                var spatilDb = dbMgr.GetDatabse(AppRoot.DB, objectID) as IGeowayWorkspace;
                var datasource = (spatilDb as Geoway.ADF.GDC.Dataset.GwGeoDatabase);
                var _spatilDBConn = DBHelper.Load(datasource.ConnectionProperties);

                {
                    var mainWorkspace = spatilDb.GeowayWorkspace;
                    Form frm = new Form() { Width = 800, Height = 1000, WindowState = FormWindowState.Maximized };
                    var setting = new DMS.UI.GrassLand.UCSchemaSettings() { Dock = DockStyle.Fill };
                    setting.Init(AppRoot.DB, _spatilDBConn, mainWorkspace);
                    frm.Controls.Add(setting);
                    frm.ShowDialog();
                }

                {
                    var frm = new Form() { Width = 800, Height = 1000, WindowState = FormWindowState.Maximized };
                    var rlt = new DMS.UI.GrassLand.UCGrassResult() { Dock = DockStyle.Fill };
                    rlt.Init(AppRoot.DB, _spatilDBConn);
                    Geoway.ADF.GIS.OpenDataDialog.GwOpenDataDialog dataDialog = new GIS.OpenDataDialog.GwOpenDataDialog();
                    dataDialog.AddFilter(GIS.OpenDataDialog.EnumOpenDataFilter.FeatureClass);
                    if (dataDialog.ShowDialog() == DialogResult.OK)
                    {
                        var gwObj = dataDialog.Selection[0] as GwGeowayShapefileObject;
                        var fc = gwObj.Dataset as Data.Geodatabase.IFeatureClass;
                        rlt.AddTempLayer(gwObj.FullName);
                    }
                    frm.Controls.Add(rlt);
                    frm.ShowDialog();
                    //return;
                }
#endif

                //验证子系统号
                if (!AppRoot.Init4_VerifySubSysMark())
                {
                    DevMessageUtil.ShowMsgWarningDialog("系统注册信息不存在，请与管理员联系！");
                    return;
                }

                //登录对话框
                FormLogin frmLogin = (FormLogin)AppRoot.GetLoginForm();
                frmLogin.Text = AppRoot.SystemTitle + "     ";
                frmLogin.UseNewRightEx = false;
                if (sysArgs.IsValid) //采用命令行参数登陆
                {
                    string error = null;
                    if (!LoginControl.Instance.Login(AppRoot.DB, sysArgs.UserName, sysArgs.Password, AppRoot.SubSysID, AppRoot.SysLoginRightCode, false, true, ref error))
                    {
                        DevMessageUtil.ShowMessageDialog(error);
                        return;
                    }
                }
                else
                {
                    //释放临时变量,实际的对象并没有释放
                    //loginUI = null;
                    //在登录对话框出来这前关闭启动splash窗口
                    frmSplash.Hide();
                    if (frmLogin.ShowDialog() != DialogResult.OK)
                    {
                        frmLogin.Dispose();
                        //--登录失败则清理资源退出
                        frmSplash.Close();
                        frmSplash = null;
                        ClearResource();
                        return;
                    }
                }

                ////是否有权限登陆系统
                //if (!LoginControl.checkRight(AppRoot.SysLoginRightCode))
                //{
                //    DevMessageUtil.ShowMsgWarningDialog("用户已注册，但未获得登陆该系统的权限！");
                //    ClearResource();
                //    return;
                //}

                //加载用户皮肤设置
                DevSkinHelper.RestoreUserSkinSetting(LoginControl.Instance.UserName);

                //让splash窗口显示出来，还要在其上显示加载的信息。
                frmSplash.Show();
                Application.DoEvents();
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                //初始化需要数据库连接的模块
                AppRoot.InitDB4Plugins(AppRoot.DB);

                //初始化主界面
                frmMain = GetMainMain();
                frmMain.Text = AppRoot.SystemTitle + "     ";//RibbonForm对中文标题支持不好
                frmMain.WindowState = FormWindowState.Maximized;


                CloseSplash();

                System.Windows.Forms.Cursor.Current = Cursors.Default;
                LoginControl.Instance.WriteLog(1, "系统登录：");
                Application.Run(frmMain);
            }
            catch (Exception ex)
            {
                AppRoot.SytemExitReason = EnumSystemExitReason.SystemException;
                LogHelper.Error.Append(ex);
                DevMessageUtil.ShowMsgWarningDialog("系统启动异常，请重新启动，或与管理员联系！");
                UnhandledException(ex);
                return;
            }
        }

        private static Form GetMainMain()
        {

            string formStyle = ConfigUtil.GetConfigValue("FormStyle", "Winform");
            if (formStyle.ToUpper() == "WPF")
            {
                WpfUtil.Instance.LoadWpfTheme();
                FormDataManageHost frmMain = new FormDataManageHost();
                frmMain.Init(AppRoot.DB);
                //frmMain.WindowState = FormWindowState.Maximized;
                return frmMain;
            }
            else
            {
                FormMainBiz frmMain = new FormMainBiz();
                frmMain.SetSplashBackMsg(frmSplash);
                frmMain.Initialize(AppRoot.DB);
                return frmMain;
            }

        }

        /// <summary>
        /// 判断程序是否是以管理员身份运行。
        /// </summary>
        private static bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private static string GetOperatorSystem()
        {
            string systemName = string.Empty;
            OperatingSystem os = Environment.OSVersion;
            switch (os.Platform)
            {
                case PlatformID.Win32Windows:
                    switch (os.Version.Minor)
                    {
                        case 0:
                            systemName = "Windows 95 ";
                            break;
                        case 10:
                            if (os.Version.Revision.ToString() == "2222A ")
                                systemName = "Windows 98 第二版 ";
                            else
                                systemName = "Windows 98 ";
                            break;
                        case 90:
                            systemName = "Windows Me ";
                            break;
                    }
                    break;
                case PlatformID.Win32NT:
                    switch (os.Version.Major)
                    {
                        case 3:
                            systemName = "Windows NT 3.51 ";
                            break;
                        case 4:
                            systemName = "Windows NT 4.0 ";
                            break;
                        case 5:
                            switch (os.Version.Minor)
                            {
                                case 0:
                                    systemName = "Windows 200 ";
                                    break;
                                case 1:
                                    systemName = "Windows XP ";
                                    break;
                                case 2:
                                    systemName = "Windows 2003 ";
                                    break;
                            }
                            break;
                        case 6:
                            switch (os.Version.Minor)
                            {
                                case 0:
                                    systemName = "Windows Vista ";
                                    break;
                                case 1:
                                    systemName = "Windows 7 ";
                                    break;
                            }
                            break;
                    }
                    break;
            }
            return systemName;
        }
        private static void OnApplicationExit(object sender, EventArgs e)
        {
            ClearResource();
            string log = "正常";
            Process p = Process.GetCurrentProcess();
            switch (AppRoot.SytemExitReason)
            {
                case EnumSystemExitReason.UserClose:
                    break;
                case EnumSystemExitReason.UserRestart:
                    Process.Start(Application.ExecutablePath);
                    break;
                case EnumSystemExitReason.SystemException:
                    log = "异常";
                    break;
                default:
                    break;
            }
            try
            {
                LoginControl.WriteToLog(2, "系统" + log + "退出：" + LoginControl.userName);
                LoginControl.Instance.Logout();
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
            p.Kill();
        }

        public static void OnThreadException(object sender, ThreadExceptionEventArgs t)
        {
            try
            {
                LogHelper.Error.Append(t.Exception);
            }
            catch (Exception)
            {

            }
        }

        [Conditional("RELEASE")]
        private static void UnhandledException(Exception ex)
        {
            string fullName = PathManagerUtil.GetMainPath() + "\\Crash.log";

            using (StreamWriter stream = new System.IO.StreamWriter(fullName, true))
            {
                stream.AutoFlush = true;
                stream.WriteLine("Time of crash: " + DateTime.Now.ToString());
                stream.WriteLine();

                Exception writeMe = ex;
                bool first = true;

                while (writeMe != null)
                {
                    if (first != true)
                    {
                        stream.WriteLine();
                        stream.Write("Inner ");
                    }

                    stream.WriteLine("Exception details:");
                    stream.WriteLine(writeMe.ToString());
                    writeMe = writeMe.InnerException;
                    first = false;
                }

                stream.WriteLine("------------------------------------------------------------------------------");
            }
        }

        private static void TraceException(Exception exp)
        {
            try
            {
                LogHelper.Error.Append(exp);
            }
            catch (Exception)
            {

            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            TraceException((Exception)e.ExceptionObject);
            UnhandledException((Exception)e.ExceptionObject);
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            TraceException(e.Exception);
            UnhandledException(e.Exception);
        }

        private static void ShowSplash()
        {
            frmSplash = new FormSplashBase();
            try
            {
                //把此图片放到编译好的可执行文件的目录下，代码不用修改了
                string sImg = "";
                sImg = "splash.jpg";
                frmSplash.LoadPicture(PathManagerUtil.GetMainPath() + @"\Img\" + sImg);
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
            frmSplash.Show();
            Application.DoEvents();

            frmSplash.CreateMsgLabel(false, 20, 20);
            frmSplash.SetMessageLabelStyle(Color.White);
            Font font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(134)));
            frmSplash.SetMessageLabelStyle(font);

            frmSplash.ShowMsg("正在启动，请稍候...");
        }

        private static bool CheckOutLicense()
        {
            //try
            //{
            //    LicenseCheck lc = new LicenseCheck(65006, -1);
            //}
            //catch (Exception ex)
            //{
            //    LogHelper.Error.Append(ex);
            //    DevMessageUtil.ShowMsgWarningDialog("您的程序未正确授权，请联系Geoway获取正版许可。");
            //    return false;
            //}
            return true;
        }

        private static void CloseSplash()
        {
            for (double d = 5; d > 1; d--)
            {
                frmSplash.Opacity = d / 5;
                frmSplash.Refresh();
                Application.DoEvents();
            }
            frmSplash.Close();
            frmSplash = null;
        }

        private static void ClearResource()
        {
            AppRoot.ClearResource();
            frmMain = null;
            try
            {
                DevSkinHelper.SaveUserSkinSetting(LoginControl.Instance.UserName);

                //释放许可连接
                //LicenseOp.DisConnect();
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
        }
    }

    public class SystemArgs
    {
        public SystemArgs()
        { }

        public SystemArgs(string[] args)
        {
            _args = args;
            ReadArgs();
        }

        string[] _args;
        bool _isValid;
        string _userName;
        string _password;
        string _dbConn;

        public string DbConnString
        {
            get { return _dbConn; }
        }

        public string[] Args
        {
            get { return _args; }
            set { _args = value; }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set { _isValid = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public void ReadArgs()
        {
            string arguments = "";
            if (_args != null && _args.Length == 1)
            {
                arguments = _args[0];
            }
            else if (_args.Length == 2)
            {
                arguments = _args[0];
                _dbConn = _args[1];
            }
            else
            {
                _isValid = false;
                return;
            }
            //读取用户信息
            string[] userInfoArgs = arguments.Split(new Char[] { ';' });
            if (userInfoArgs.Length >= 2)
            {
                _userName = userInfoArgs[0];
                _password = userInfoArgs[1];

                _isValid = true;
            }
            else
            {
                _isValid = false;
            }
        }
    }

}