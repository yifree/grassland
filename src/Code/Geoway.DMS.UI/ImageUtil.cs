﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Geoway.DMS.UI
{
    public static class ImageUtil
    {
        public static Image LoadImage(string name)
        {
            string file = Path.Combine(Application.StartupPath, "images", $"{name}.png");
            if (File.Exists(file))
                return Image.FromFile(file);
            return null;
        }
    }
}
