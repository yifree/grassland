using Geoway.ADF.Core.Utility;
using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Basic.SpatialReference;
using Geoway.Data.Geodatabase;
using System;
using System.Collections.Generic;

namespace Geoway.DMS.UI
{
    public class FeatureCopy
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcFC"></param>
        /// <param name="trgFC"></param>
        /// <param name="srcFilter"></param>
        /// <param name="stepSize"></param>
        /// <param name="onStepChanged"></param>
        /// <param name="onError"></param>
        /// <param name="onInserted">newOid,oldFeature</param>
        /// <returns></returns>
        public static bool CopyFeature(IFeatureClass srcFC, IFeatureClass trgFC, IFilter srcFilter, int stepSize = 50,
            Action<int> onStepChanged = null, Action<int, string> onError = null,
            Action<IFeature, IFeatureBuffer> onBeforeInsert = null,
            Action<int, IFeatureBuffer> onInserted = null)
        {
            IFeatureInsertCursor trgCursor = null;
            IFeatureSearchCursor srcCursor = null;

            try
            {
                ISpatialReferenceSystem srcRef = srcFC.spatialReferenceSystem;
                ISpatialReferenceSystem trgRef = trgFC.spatialReferenceSystem;

                srcCursor = srcFC.Search(srcFilter, false);

                if (VectorDataOperator.StartEdit(trgFC) == false)
                {
                    return false;//打开编辑失败    
                }

                IFeature srcFeature = null;
                IFeatureBuffer trgFBuffer = null;
                bool saveSuccess = true;

                List<IField> attFields = FieldUtil.GetCustomFields(trgFC);
                trgCursor = trgFC.Insert(true);

                int count = 0;
                bool save = true;
                while ((srcFeature = srcCursor.NextFeature()) != null)
                {
                    try
                    {
                        trgFBuffer = trgFC.CreateFeatureBuffer();
                        var pNewShape = GeometryUtil.ProjectToCopy(srcFeature.Geometry, srcRef, trgRef);
                        trgFBuffer.Geometry = pNewShape;

                        foreach (var fld in attFields)
                        {
                            int srcIndex = srcFeature.Fields.FindField(fld.Name);
                            if (srcIndex < 0) continue;
                            int trgIndex = trgFBuffer.Fields.FindField(fld.Name);
                            if (trgIndex < 0) continue;

                            if (fld.Name == "f_checked")
                            {

                            }

                            trgFBuffer[trgIndex] = srcFeature[srcIndex];

                        }

                        int oid = -1;
                        onBeforeInsert?.Invoke(srcFeature, trgFBuffer);
                        saveSuccess = trgCursor.InsertFeature(trgFBuffer, out oid);
                        if (saveSuccess)
                        {
                            onInserted?.Invoke(oid, trgFBuffer);
                            count++;
                            if ((count % stepSize) == 0)
                                onStepChanged?.Invoke(count);
                        }
                        else
                        {
                            throw new Exception("写入失败");
                        }
                    }
                    catch (Exception ex)
                    {
                        onError(srcFeature.OID, ex.Message);
                        save = false;
                        LogHelper.Error.Append(ex);
                        break;
                    }
                }
                if (count > 0 && (count % stepSize) != 0)
                {
                    onStepChanged?.Invoke(count);
                }

                ((IInsertCursor)trgCursor).Flush();

                //提交事物之前必须先释放cursor
                ReferenceReleaser.ReleaseObject(trgCursor);
                VectorDataOperator.StopEdit(trgFC, save);

                if (save)
                {
                    trgFC.UpdateExtent();
                }
                return save;
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);

                ReferenceReleaser.ReleaseObject(trgCursor);
                VectorDataOperator.StopEdit(trgFC, false);
                return false;
            }
        }
    }
}