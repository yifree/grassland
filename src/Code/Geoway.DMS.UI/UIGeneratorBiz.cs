﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using Geoway.ADF.Core.Config;
using Geoway.ADF.Core.DB;
using Geoway.ADF.Core.Utility;
using Geoway.ADF.GDC.Dataset;
using Geoway.ADF.GDC.GeowayDataset;
using Geoway.Data.Geodatabase;
using Geoway.DMS.UI.GrassLand;
using Geoway.GDC.DMS.UI;

namespace Geoway.DMS.UI
{
    /// <summary>
    /// 项目定制功能构造类
    /// </summary>
    public class UIGeneratorBiz : UIGenerator
    {
        private UCSchemaSettings ucSchemaSettings;
        private UCGrassResult ucGrassResult;
        private IWorkspace mainWorkspace;
        private IDBHelper _spatilDBConn;

        public UIGeneratorBiz(IDBHelper db, RibbonControl ribbon, PanelControl container)
            : base(db, ribbon, container)
        {
            OpenMainWorkspace();
        }

        void OpenMainWorkspace()
        {
            try
            {
                var objectID = ObjectUsageHelper.GetUniqueUsageObject(_db, "MainGeoDatabase");
                var dbMgr = new GeoDatabaseManage();
                var spatilDb = dbMgr.GetDatabse(_db, objectID) as IGeowayWorkspace;
                var datasource = (spatilDb as Geoway.ADF.GDC.Dataset.GwGeoDatabase);
                _spatilDBConn = DBHelper.Load(datasource.ConnectionProperties);
                mainWorkspace = spatilDb.GeowayWorkspace;
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
        }

        public void InitGrassSchema(BarButtonItem btnGrassSchema)
        {
            btnGrassSchema.ItemClick += (s, e) =>
            {
                if (mainWorkspace == null)
                {
                    DevMsg.Error("空间数据库打开失败");
                    return;
                }
                if (ucSchemaSettings == null)
                {
                    ucSchemaSettings = new UCSchemaSettings() { Dock = DockStyle.Fill };
                    ucSchemaSettings.OnLayerSaved += UcSchemaSettings_OnLayerSaved;
                    ucSchemaSettings.Init(_db, _spatilDBConn, mainWorkspace);
                    AppendUC2Container(ucSchemaSettings);
                }
                Bring2Front(ucSchemaSettings);
            };
        }

        private void UcSchemaSettings_OnLayerSaved(IFeatureClass obj, string shapefile)
        {
            OpenGrassResult(obj, shapefile);
        }

        public void InitGrassResult(BarButtonItem btnGrassResult)
        {
            btnGrassResult.ItemClick += (s, e) =>
            {
                OpenGrassResult(null, null);
            };
        }

        private void OpenGrassResult(IFeatureClass featureClass, string shapefile)
        {
            if (mainWorkspace == null)
            {
                DevMsg.Error("空间数据库打开失败");
                return;
            }
            if (ucGrassResult == null)
            {

                try
                {
                    GwWaitForm.Start("加载样地结果...");
                    ucGrassResult = new UCGrassResult() { Dock = DockStyle.Fill };
                    ucGrassResult.Init(_db, _spatilDBConn);
                }
                catch (Exception ex)
                {
                    LogHelper.Error.Append(ex);
                }
                finally
                {
                    GwWaitForm.Stop();
                }
                AppendUC2Container(ucGrassResult);
            }
            if (featureClass != null)
            {
                ucGrassResult.AddTempLayer(shapefile);
            }
            Bring2Front(ucGrassResult);
        }
    }
}
