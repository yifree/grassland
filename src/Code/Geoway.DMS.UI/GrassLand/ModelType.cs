﻿using System.ComponentModel;

namespace Geoway.DMS.UI.GrassLand
{
    public enum ModelType
    {
        [Description("最大面积法")]
        MaxArea = 0x01,
        [Description("平均距离法")]
        AvgDistance = 0x02,
    }
}
