﻿using Geoway.ADF.Core.Controls;
using Geoway.ADF.Core.Utility;
using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Basic.Algorithm;
using Geoway.Basic.Geometry;
using Geoway.Data.Geodatabase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Geoway.DMS.UI.GrassLand
{
    public abstract class SettingImpBase
    {
        protected ILogDoubleProgressFeedback Log;
        protected SampleSetting settingInfo;

        public IFeatureClass Export(SampleSetting setting, IFeatureClass source, ILogDoubleProgressFeedback log, out string outpath)
        {
            Log = log;
            outpath = null;
            settingInfo = setting;
            string folder = settingInfo.Path;
            var wks = WorkspaceUtil.OpenShapefileWorkspace(folder) as IFeatureWorkspace;
            string outputName = settingInfo.GetOutputName();
            try
            {
                var distItem = settingInfo.DistrictList[0];
                List<string> where = new List<string>(){
                        $"{SampleSetting.FLD_XZQ} LIKE '{StringUtil.getDistCodePart(distItem.Code)}%'",
                        $"{ SampleSetting.FLD_MJ}>={settingInfo.MinAreaMeters}"
                    };
                var filter = new LiteralConditionClass();
                filter.WhereClause = String.Join(" AND ", where);
                var total = source.FeatureCount(new LiteralConditionClass() { WhereClause = filter.WhereClause });
                Log.SetCurrentProgressMax(total);
                outpath = Path.Combine(folder, outputName);

                var fields = new FieldsEditClass();
                fields.AddField(FieldUtil.CreateOIDField("OBJECTID", "OBJECTID"));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_YEAR, "年份", Basic.System.DataType.Int32, true));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_JCDBH, "监测点编号", Basic.System.DataType.String, true));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_XZQ, "地域编码", Basic.System.DataType.String, true));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_LON, "经度", Basic.System.DataType.Double, false));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_LAT, "纬度", Basic.System.DataType.Double, false));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_CLASS, "类中", Basic.System.DataType.String, true));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_TYPE, "型中", Basic.System.DataType.String, true));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_NEWCLASS, "新草地类", Basic.System.DataType.String, true));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_NEWTYPE, "新草地型", Basic.System.DataType.String, true));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_MJ, "图斑面积", Basic.System.DataType.Double, false));
                fields.AddField(FieldUtil.CreateField(SampleSetting.FLD_STATECODE, SampleSetting.FLD_STATECODE, Basic.System.DataType.Int32, false));
                fields.AddField(FieldUtil.CreateShapeField("Shape", "Shape", GeometryType.SimplePolygon));

                var dest = wks.CreateFeatureClass(outputName, fields, source.spatialReferenceSystem, FeatureType.Polygon, source.ShapeFieldName, null);
                //var dest = VectorDataOperator.CreateFeatureClass(source, null, wks, outputName, source.OIDFieldName, source.ShapeFieldName, source.spatialReferenceSystem);
                if (dest == null)
                {
                    throw new Exception("创建输出要素类失败");
                }
                Log.AppendLine("开始输出图斑...");
                List<UpdateFeatureInfo> allFeatures = new List<UpdateFeatureInfo>();

                var fldIndex_JCDBH = dest.FindField(SampleSetting.FLD_JCDBH);
                var fldIndex_XZQ = dest.FindField(SampleSetting.FLD_XZQ);
                //var fldIndex_MJ = dest.FindField(SampleSetting.FLD_MJ);
                var fldIndex_OldClass = dest.FindField(SampleSetting.FLD_CLASS);
                var fldIndex_OldType = dest.FindField(SampleSetting.FLD_TYPE);
                var fldIndex_NewClass = dest.FindField(SampleSetting.FLD_NEWCLASS);
                var fldIndex_NewType = dest.FindField(SampleSetting.FLD_NEWTYPE);

                var fldIndexDYBM = dest.FindField(SampleSetting.FLD_DYBM);
                var fldIndexYear = dest.FindField(SampleSetting.FLD_YEAR);
                var fldIndexLon = dest.FindField(SampleSetting.FLD_LON);
                var fldIndexLat = dest.FindField(SampleSetting.FLD_LAT);

                int count = 0;
                var success = FeatureCopy.CopyFeature(source, dest, filter, 50,
                     (pos) =>
                     {
                         Log.SetCurrentProgressPosition(pos);
                         Log.AppendLine($"已经输出 {pos} 个图斑");
                     },
                     (oid, err) => { Log.AppendLine($"ID为 {oid} 的图斑导出失败"); },
                     onBeforeInsert: (fea, buffer) =>
                     {
                         count++;
                         var xzq = buffer.GetValue(fldIndex_XZQ).ToString().Substring(0, 6);
                         var jcdbh = $"{xzq}{count.ToString().PadLeft(6, '0')}";
                         buffer[fldIndex_JCDBH] = FieldUtil.Convert2GeowayDataValue(Basic.System.DataType.String, jcdbh);
                         buffer[fldIndexDYBM] = FieldUtil.Convert2GeowayDataValue(Basic.System.DataType.String, xzq);
                         buffer[fldIndexYear] = FieldUtil.Convert2GeowayDataValue(Basic.System.DataType.Int32, DateTime.Now.Year);
                         var polygon = fea.Geometry as IPolygon;
                         if (polygon == null)
                         {
                             IGeometry geometry = null;
                             IGeometryCollection geometryCollection = fea.Geometry as IGeometryCollection;
                             for (int i = 0; i < geometryCollection.Size; i++)
                             {
                                 var geom = geometryCollection.GetGeometry(i);
                                 if (geometry == null)
                                     geometry = geom;
                                 else
                                     geometry = SpatialAnalysisFuncs.Union(geom, geometry);
                             }
                             polygon = geometry as IPolygon;
                         }
                         if (polygon != null)
                         {
                             var center = PolygonFuncs.GetCentroid(polygon);
                             buffer[fldIndexLon] = FieldUtil.Convert2GeowayDataValue(Basic.System.DataType.Double, Math.Round(center.X, 6));
                             buffer[fldIndexLat] = FieldUtil.Convert2GeowayDataValue(Basic.System.DataType.Double, Math.Round(center.Y, 6));
                         }

                         var f_check_index = dest.FindField(SampleSetting.FLD_STATECODE);
                         if (f_check_index >= 0)
                         {
                             var isOptional =
                             (!settingInfo.OldClasses.Any() || settingInfo.OldClasses.Contains(buffer[fldIndex_OldClass]?.ToString())) &&
                             (!settingInfo.OldTypes.Any() || settingInfo.OldTypes.Contains(buffer[fldIndex_OldType]?.ToString())) &&
                             (!settingInfo.NewClasses.Any() || settingInfo.NewClasses.Contains(buffer[fldIndex_NewClass]?.ToString())) &&
                             (!settingInfo.NewTypes.Any() || settingInfo.NewTypes.Contains(buffer[fldIndex_NewType]?.ToString()));
                             var f_check = dest.Fields.getField(f_check_index);
                             if (isOptional)
                             {
                                 buffer[f_check_index] = AutoIntValue.IntValue(f_check, SampleSetting.STATE_CODE_OPTIONAL);
                             }
                             else
                             {
                                 buffer[f_check_index] = AutoIntValue.IntValue(f_check, SampleSetting.STATE_CODE_AREA_OK);
                             }
                         }
                     },
                  onInserted: (oid, fea) =>
                  {
                      var isOptional =
                         (!settingInfo.OldClasses.Any() || settingInfo.OldClasses.Contains(fea[fldIndex_OldClass]?.ToString())) &&
                         (!settingInfo.OldTypes.Any() || settingInfo.OldTypes.Contains(fea[fldIndex_OldType]?.ToString())) &&
                         (!settingInfo.NewClasses.Any() || settingInfo.NewClasses.Contains(fea[fldIndex_NewClass]?.ToString())) &&
                         (!settingInfo.NewTypes.Any() || settingInfo.NewTypes.Contains(fea[fldIndex_NewType]?.ToString()));
                      if (isOptional)
                      {
                          try
                          {
                              allFeatures.Add(new UpdateFeatureInfo()
                              {
                                  OID = oid,
                                  FeatureBuffer = fea,
                                  Lon = Convert.ToDouble(fea[fldIndexLon].ToString()),
                                  Lat = Convert.ToDouble(fea[fldIndexLat].ToString())
                              });
                          }
                          catch (Exception ex)
                          {
                              LogHelper.Error.Append(ex);
                          }
                      }
                  });
                if (success)
                {
                    var f_check_index = dest.FindField(SampleSetting.FLD_STATECODE);
                    if (f_check_index >= 0)
                    {
                        //var f_check = dest.Fields.getField(f_check_index);
                        Log.AppendLine("更新样地状态...");
                        var ctx = new Context() { Source = source, Dest = dest, Features = allFeatures };
                        UpdateResult(ctx);
                        Log.AppendLine("更新样地状态完毕");
                    }
                    else
                        LogHelper.Error.Append($"图层{outputName}未找到{SampleSetting.FLD_STATECODE}字段");
                    //ReferenceReleaser.ReleaseObject(dest);
                    //ReferenceReleaser.ReleaseObject(wks);
                    Log.AppendLine($"输出成功");
                    return dest;
                }
                else
                {
                    string name = dest.Name;
                    ReferenceReleaser.ReleaseObject(dest);
                    VectorDataOperator.DeleteFeatureClass(wks, name);
                    ReferenceReleaser.ReleaseObject(wks);
                    Log.AppendLine($"输出失败.");

                    try
                    {
                        dest?.Delete();
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error.Append(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.AppendLine($"输出程序错误.");
                LogHelper.Error.Append(ex);
            }
            return null;
        }

        protected abstract bool UpdateResult(Context context);

        protected class Context
        {
            public IFeatureClass Source { get; set; }

            public IFeatureClass Dest { get; set; }

            public List<UpdateFeatureInfo> Features { get; set; }
        }

        protected class UpdateFeatureInfo
        {
            public int OID { get; set; }

            public IFeatureBuffer FeatureBuffer { get; set; }

            public double Lon { get; set; }

            public double Lat { get; set; }
        }
    }
}
