﻿using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Logic.Carto;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Geoway.DMS.UI.GrassLand
{
    public static class RenderUtilEx
    {
        public static void UniqueValueRender(IFeatureLayer layer, string field, ILayerState layerState, Dictionary<string, Color> colors)
        {
            layerState.DisplayState = DisplayState.ByClass;
            layerState.Symbolizing = false;

            IFeatureRenderStrategy renderStrategy = layer.GetActiveStrategyByDisplayState(layerState.DisplayState);
            renderStrategy.RemoveAllSymbolClass();
            IGeoThemeStrategy geoStrategy = renderStrategy as IGeoThemeStrategy;
            geoStrategy.UniqueValueFieldName = field;

            var geometryType = RenderUtil.ReadFeatureRendLayerType(layer);

            foreach (var colorPair in colors)
            {
                ISubClass subClass = new SubClassClass(colorPair.Key);
                subClass.ClassID = colorPair.Key;
                subClass.Name = colorPair.Key;
                subClass.FeatureType = layer.FeatureClass.FeatureType;

                //设置符号
                var symbolPro = RenderUtil.GetSymbolByColor(Color.Transparent, colorPair.Value, geometryType);                
                RenderUtil.SetSymbolProperty(subClass.DisplayStyle, symbolPro, geometryType);
                subClass.FeatureType = layer.FeatureClass.FeatureType;

                layerState.AddGeoClass(subClass.ClassID, layerState.LayerVisible);
                renderStrategy.AddSymbolClass(subClass, 0);
            }
        }
    }
}
