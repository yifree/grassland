﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Geoway.ADF.Core.DB;
using Geoway.ADF.Core.Controls;
using Geoway.ADF.Core.Utility;
using System.IO;
using DevExpress.XtraEditors.Controls;
using Geoway.Data.Geodatabase;
using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Basic.Geometry;
using Geoway.Basic.Algorithm;

namespace Geoway.DMS.UI.GrassLand
{
    /// <summary>
    /// 参数设置
    /// </summary>
    public partial class UCSchemaSettings : XtraUserControl
    {
        private GwLogProgress Log;
        private SampleSetting settingInfo = new SampleSetting();
        private IDBHelper _db;
        private IDBHelper _dbSpatial;
        private IWorkspace workspace;
        public event Action<IFeatureClass, string> OnLayerSaved;

        public UCSchemaSettings()
        {
            InitializeComponent();
            Log = new GwLogProgress() { Dock = DockStyle.Fill };
            Log.TotalProgressVisible = false;
            Log.MessagePanelVisible = false;
            gpLog.Controls.Add(Log);

            DevControlUtil.SetEditorInteger(ref txtSampleCount, false, false, 10);
            DevControlUtil.SetEditorDouble(ref txtMinArea, false, false, 10, 2);

            cmbModel.Properties.Items.Add(new ItemInfo(ModelType.MaxArea, SampleSetting.ModelType2Name(ModelType.MaxArea)));
            cmbModel.Properties.Items.Add(new ItemInfo(ModelType.AvgDistance, SampleSetting.ModelType2Name(ModelType.AvgDistance)));
            cmbModel.SelectedIndex = 0;
        }


        public void Init(IDBHelper db, IDBHelper dbSpatial, IWorkspace workspace)
        {
            _db = db;
            _dbSpatial = dbSpatial;
            this.workspace = workspace;
            ucxzqh.Init(db);
            txtSampleCount.Text = "100";

            LoadCodeList("grass_lz.json", SampleSetting.FLD_CLASS, SampleSetting.FLD_CLASS, chkCmbClass);
            LoadCodeList("grass_xz.json", SampleSetting.FLD_TYPE, SampleSetting.FLD_TYPE, chkCmbType);
            LoadCodeList("grass_xl.json", SampleSetting.FLD_NEWCLASS, SampleSetting.FLD_NEWCLASS, chkCmbNewClass);
            LoadCodeList("grass_xx.json", SampleSetting.FLD_NEWTYPE, SampleSetting.FLD_NEWTYPE, chkCmbNewType);
        }

        private List<ClassInfo> LoadCodeList(string file, string field, string code, CheckedComboBoxEdit edit)
        {
            string fileName = Path.Combine(Application.StartupPath, file);
            List<ClassInfo> result = null;
            if (File.Exists(fileName))
            {
                var json = File.ReadAllText(fileName, Encoding.UTF8);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClassInfo>>(json);
            }
            else
            {
                var codeFld = code == null ? (field + " as " + code) : code;
                var dt = _dbSpatial.DoQueryEx($"select distinct {field}, {codeFld} from {SampleSetting.TB_DLTB} where {field} is not null");
                if (dt != null)
                {
                    var lst = from row in dt.AsEnumerable()
                              select new ClassInfo()
                              {
                                  Name = row[field]?.ToString(),
                                  Code = row[code]?.ToString()
                              };
                    result = lst.ToList();
                    var settings = new Newtonsoft.Json.JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                        TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Auto,
                        //Formatting= Newtonsoft.Json.Formatting.Indented,
                    };
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(lst, settings);
                    File.WriteAllText(fileName, json, Encoding.UTF8);
                }
            }
            if (result == null)
                result = new List<ClassInfo>();

            if (edit != null)
            {
                edit.Properties.Items.Clear();
                edit.Properties.Items.AddRange(result.ToArray());
            }
            return result;
        }

        private void BtnPath_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.ShowNewFolderButton = true;
            folderBrowser.Description = "选择或创建输出文件夹";
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                if (Directory.GetFileSystemEntries(folderBrowser.SelectedPath).Length > 0)
                {
                    DevMsg.Warning("警告：输出文件夹不为空，可能无法输出");
                }
                btnPath.Text = folderBrowser.SelectedPath;
            }
        }

        private void BtnPath_DoubleClick(object sender, EventArgs e)
        {
            BtnPath_ButtonClick(null, null);
        }

        IEnumerable<T> GetCheckedItems<T>(CheckedComboBoxEdit edit)
        {
            foreach (CheckedListBoxItem item in edit.Properties.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    yield return (T)item.Value;
                }
            }
        }

        private void ShowLogInfo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateParams(true))
                    return;

                Log.ClearLog();
                var whereDL = $"{SampleSetting.FLD_DLBM} like '04%'";
                foreach (var dist in settingInfo.DistrictList)
                {
                    var whereXZQ = $"{SampleSetting.FLD_XZQ} LIKE '{StringUtil.getDistCodePart(dist.Code)}%'";
                    var cnt = (long)(_dbSpatial.QueryScalar($"SELECT COUNT(1) FROM {SampleSetting.TB_DLTB} WHERE {whereXZQ}"));
                    Log.AppendLine($"{dist.Name} 三调草地图斑个数 {cnt} 个");

                    var whereArea = $"{ SampleSetting.FLD_MJ}>={settingInfo.MinAreaMeters}";
                    cnt = (long)(_dbSpatial.QueryScalar($"SELECT COUNT(1) FROM {SampleSetting.TB_DLTB} WHERE {whereXZQ} AND {whereArea}"));
                    Log.AppendLine($"图斑面积>= {settingInfo.MinArea} 亩的三调草地图斑个数 {cnt} 个");
                    btnSaveResult.Enabled = cnt >= settingInfo.SampleCount;

                    string whereClass = null;
                    string infoClass = null;
                    if (settingInfo.OldClasses.Length > 0)
                    {
                        whereClass = $"{ SampleSetting.FLD_CLASS_CODE} in ({string.Join(",", settingInfo.OldClasses.Select(r => $"'{r}'"))})";
                        infoClass = $"类中为 {string.Join("、", settingInfo.OldClasses)}";
                        cnt = (long)(_dbSpatial.QueryScalar($"SELECT COUNT(1) FROM {SampleSetting.TB_DLTB} WHERE {whereXZQ} AND {whereArea} AND {whereClass}"));
                        Log.AppendLine($"图斑面积>= {settingInfo.MinArea} 亩，{infoClass} 的三调草地图斑个数 {cnt} 个");
                    }

                    string whereTypes = null, infoTypes = null;
                    if (settingInfo.OldTypes.Length > 0)
                    {
                        whereTypes = $"{ SampleSetting.FLD_TYPE_CODE} in ({string.Join(",", settingInfo.OldTypes.Select(r => $"'{r}'"))})";
                        infoTypes = $"型中为 {string.Join("、", settingInfo.OldTypes)}";
                        cnt = (long)_dbSpatial.QueryScalar($"SELECT COUNT(1) FROM {SampleSetting.TB_DLTB} WHERE {whereXZQ} AND {whereArea} AND {whereTypes}");
                        Log.AppendLine($"图斑面积>= {settingInfo.MinArea} 亩，{infoTypes} 的三调草地图斑个数 {cnt} 个");
                    }

                    string whereNewClass = null, infoNewClass = null;
                    if (settingInfo.NewClasses.Length > 0)
                    {
                        whereNewClass = $"{ SampleSetting.FLD_NEWCLASS_CODE} in ({string.Join(",", settingInfo.NewClasses.Select(r => $"'{r}'"))})";
                        infoNewClass = $"新草地类为 {string.Join("、", settingInfo.NewClasses)}";
                        cnt = (long)_dbSpatial.QueryScalar($"SELECT COUNT(1) FROM {SampleSetting.TB_DLTB} WHERE {whereXZQ} AND {whereArea} AND {whereNewClass}");
                        Log.AppendLine($"图斑面积>= {settingInfo.MinArea} 亩，{infoNewClass} 的三调草地图斑个数 {cnt} 个");
                    }

                    string whereNewTypes = null, infoNewTypes = null;
                    if (settingInfo.NewTypes.Length > 0)
                    {
                        whereNewTypes = $"{ SampleSetting.FLD_NEWTYPE_CODE} in ({string.Join(",", settingInfo.NewTypes.Select(r => $"'{r}'"))})";
                        infoNewTypes = $"新草地型为 {string.Join("、", settingInfo.NewTypes)}";
                        cnt = (long)_dbSpatial.QueryScalar($"SELECT COUNT(1) FROM {SampleSetting.TB_DLTB} WHERE {whereXZQ} AND {whereArea} AND {whereNewTypes}");
                        Log.AppendLine($"图斑面积>= {settingInfo.MinArea} 亩，{infoNewTypes} 的三调草地图斑个数 {cnt} 个");
                    }

                    {
                        Dictionary<string, string> literal = new Dictionary<string, string>();
                        if (whereClass != null)
                            literal.Add(whereClass, infoClass);
                        if (whereTypes != null)
                            literal.Add(whereTypes, infoTypes);
                        if (whereNewClass != null)
                            literal.Add(whereNewClass, infoNewClass);
                        if (whereNewTypes != null)
                            literal.Add(whereNewTypes, infoNewTypes);
                        if (literal.Count > 1)
                        {
                            cnt = (long)(_dbSpatial.QueryScalar($"SELECT COUNT(1) FROM {SampleSetting.TB_DLTB} WHERE {whereXZQ} AND {whereArea} AND {string.Join(" AND ", literal.Keys)}"));
                            Log.AppendLine($"图斑面积>= {settingInfo.MinArea} 亩，{string.Join("，", literal.Values)} 的三调草地图斑个数 {cnt} 个");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DevMsg.Error("查询数据出错：" + ex.Message);
                LogHelper.Error.Append(ex);
            }
        }

        private void BtnSaveResult_Click(object sender, EventArgs e)
        {
            if (!ValidateParams())
                return;
            IFeatureClass source = VectorDataOperator.OpenFeatureClass(workspace, SampleSetting.TB_DLTB);
            SettingImpBase settingImp = SettingImpFactory.Create(settingInfo.ModelType);
            var output = settingImp.Export(settingInfo, source, Log, out string outpath);
            if (output != null && OnLayerSaved != null && DevMsg.YesNo("输出成功！是否立即进行样地调整？") == DialogResult.Yes)
            {
                OnLayerSaved?.Invoke(output, outpath);
            }
        }

        bool ValidateParams(bool pathNullable = false)
        {
            var dist = ucxzqh.Districts;
            if (dist.Count == 0)
            {
                DevMsg.Error("行政区划未选择，请重新进行样地参数设置"); ucxzqh.Focus();
                return false;
            }
            settingInfo.DistrictList = dist;
            settingInfo.ModelType = (ModelType)(cmbModel.SelectedItem as ItemInfo).Objectvalue;

            if (!int.TryParse(txtSampleCount.Text, out int count) || count <= 0)
            {
                DevMsg.Error("样地数量应为大于0的数字"); txtSampleCount.Focus();
                return false;
            }
            settingInfo.SampleCount = count;

            if (!double.TryParse(txtMinArea.Text, out double area) || area <= 0)
            {
                DevMsg.Error("图斑面积应为大于0的数字"); txtMinArea.Focus();
                return false;
            }

            if (!pathNullable)
            {
                string outName = settingInfo.GetOutputName();
                if (string.IsNullOrEmpty(btnPath.Text) || Directory.GetFileSystemEntries(btnPath.Text, outName + "*").Length > 0)
                {
                    DevMsg.Error($"输出文件 {outName} 已经存在，请选择其他输出路径");
                    return false;
                }
                settingInfo.Path = btnPath.Text;
            }

            settingInfo.MinArea = area;

            settingInfo.OldClasses = GetCheckedItems<ClassInfo>(chkCmbClass).Select(r => r.Code).ToArray();

            settingInfo.OldTypes = GetCheckedItems<ClassInfo>(chkCmbType).Select(r => r.Code).ToArray();

            settingInfo.NewClasses = GetCheckedItems<ClassInfo>(chkCmbNewClass).Select(r => r.Code).ToArray();

            settingInfo.NewTypes = GetCheckedItems<ClassInfo>(chkCmbNewType).Select(r => r.Code).ToArray();


            return true;
        }

        class ClassInfo
        {
            public string Name { get; set; }

            public string Code { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }
    }
}
