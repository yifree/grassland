﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Geoway.ADF.Core.DB;
using Geoway.ADF.GDC.Catalog;
using Geoway.ADF.MIS.Business;
using Geoway.Data.Geodatabase;
using Geoway.ADF.GDC.CatalogUI;
using Geoway.ADF.Core.Framework;
using Geoway.ADF.GIS.GeowayCatalog;
using Geoway.Logic.Carto;

namespace Geoway.DMS.UI.GrassLand
{
    public partial class UCGrassResult : XtraUserControl
    {
        private UCGwCatalogControl mainCatalog;
        private IDBHelper _db;

        public UCGrassResult()
        {
            InitializeComponent();
            Map = new UCMap() { Dock = DockStyle.Fill };
            this.splitContainerControl1.Panel2.Controls.Add(Map);
        }

        public void Init(IDBHelper db, IDBHelper dbSpatial)
        {
            _db = db;
            Map.Init(db, dbSpatial);
            var mapControl = Map.MapPreview.MapControl;
            GwCatalogManage manage = new GwCatalogManage(db);

            //数据目录
            mainCatalog = new UCGwCatalogControl() { Dock = DockStyle.Fill };
            {
                var catalog = manage.GetCatalog("样地设计");
                if (catalog == null)
                {
                    catalog = new GwCatalog(db);
                    catalog.CatalogName = "样地设计";
                    catalog.Insert();
                    manage.Refresh();
                    catalog = manage.GetCatalog("样地设计");
                }
                mainCatalog.SetBuddyControl(mapControl);
                mainCatalog.InitObjects(db, catalog);
                mainCatalog.Event_AfterDeleteCatalogDataNode += MainCatalog_Event_AfterDeleteCatalogDataNode;
                mainCatalog.Evnet_BeforeCatalogNodeAddedToTree += MainCatalog_Evnet_BeforeCatalogNodeAddedToTree;
                mainCatalog.Event_AfterLoadCatalogDataNode += Map.OnLayerLoaded;
                mainCatalog.Event_BeforePopupMenu += MainCatalog_Event_BeforePopupMenu;
            }

            pageData.Controls.Add(mainCatalog);

            ////背景目录
            //List<int> userCatalogID = UserCatalogConfig.GetUserCatalogID(db, LoginControl.Instance.UserID, EnumGwCatalogUsage.Background);
            //if (userCatalogID != null && userCatalogID.Count > 0)
            //{
            //    var catalog = manage.GetCatalog(userCatalogID[0]);
            //    var ctl = new UCGwCatalogControl();
            //    ctl.SetBuddyControl(mapControl);
            //    ctl.InitObjects(db, catalog);
            //    pageBgCatalog.Controls.Add(ctl);
            //}
            //else
            //    pageBgCatalog.PageVisible = false;

            //行政区
            {
                var userCatalogID = UserCatalogConfig.GetUserCatalogID(db, LoginControl.Instance.UserID, EnumGwCatalogUsage.District);
                if (userCatalogID != null && userCatalogID.Count > 0)
                {
                    var ucCountyTree = new UCDistrictTree();
                    ucCountyTree.Initialize(db);
                    ucCountyTree.LoadDistrict(userCatalogID[0].ToString());
                    ucCountyTree.GetType().GetField("_mapControl", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
                        .SetValue(ucCountyTree, mapControl);
                    ucCountyTree.Dock = DockStyle.Fill;
                    pageXZQH.Controls.Add(ucCountyTree);
                }
                else
                    pageXZQH.PageVisible = false;
            }

            Map.MainCatalog = mainCatalog;
            Map.MapPreview.MapControl.FullExtent();
        }



        public void AddTempLayer(string shapefile)
        {
            var datasetCreator = new ADF.GDC.GeowayDataset.GeowayDatasetCreator();
            var dataset = datasetCreator.CreateGwDataset(EnumGwDatasetType.FeatureClass, EnumGwDataSourceType.Shapefile, shapefile);
            var factory = new GwGeowayCatalogNodeFactory();
            var node = factory.CreateGwCatalogDataNode(_db, dataset);
            node.NodeName = node.DataSet.Name;
            node.CanCheck = true;
            var catalogNode = node as IGwCatalogNode;
            node.Catalog = mainCatalog.Catalog;

            var name = node.NodeName;
            var exists = mainCatalog.Catalog.TempCatalogNode.GetAllChildCatalogNode(EnumGwCatalogNodeType.enumLayerNode)
                .Exists(r => r.NodeName == name);
            var cnt = 0;
            while (exists)
            {
                name += "_" + (++cnt);
                exists = mainCatalog.Catalog.TempCatalogNode.GetAllChildCatalogNode(EnumGwCatalogNodeType.enumLayerNode)
                .Exists(r => r.NodeName == name);
            }
            node.NodeName = name;

            this.mainCatalog.AddTempNode(mainCatalog.Catalog.TempCatalogNode, ref catalogNode);
            mainCatalog.ExpandCurrentNode(mainCatalog.Catalog.TempCatalogNode);
            catalogNode.Checked = true;
        }

        private void MainCatalog_Event_BeforePopupMenu(object sender, CatalogTreeNodeMouseEventArgs e)
        {
            if (e.CatalogNode != null)
            {
                var menu = mainCatalog.GetCatalogNodePopup(e.CatalogNode);
                foreach (DevExpress.XtraBars.BarItemLink item in menu.ItemLinks)
                {
                    if (item.Item.Name == "btnDisconnectLayer" || item.Item.Name == "btnDelTempLayer")
                    {
                        item.Visible = false;
                    }
                }
            }
        }

        private void MainCatalog_Evnet_BeforeCatalogNodeAddedToTree(object sender, BeforeCatalogNodeAddedToTreeEventArgs e)
        {
            if (e.CurNode.NodeType == EnumGwCatalogNodeType.enumLayerNode)
            {
                var layer = e.CurNode as IGwFeatureLayer;
                if (layer != null && layer.DataNodeType == EnumGwCatalogNodeTypeSet.enumFeatureLayerPolygon)
                {
                    Map.LayerAdded(layer);
                }
                e.ParentTreeNode?.ExpandAll();
            }
        }

        private void MainCatalog_Event_AfterDeleteCatalogDataNode(object sender, AfterCatalogNodeAddedToTreeEventArgs e)
        {
            if (e.DeleteNode.NodeType == EnumGwCatalogNodeType.enumLayerNode)
            {
                var layer = e.DeleteNode as IGwFeatureLayer;
                if (layer != null && layer.DataNodeType == EnumGwCatalogNodeTypeSet.enumFeatureLayerPolygon)
                {
                    Map.LayerRemoved(layer);
                }
            }
        }

        internal UCMap Map { get; }
    }
}
