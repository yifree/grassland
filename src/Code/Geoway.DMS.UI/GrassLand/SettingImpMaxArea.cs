﻿using Geoway.ADF.Core.Utility;
using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Data.Geodatabase;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Geoway.DMS.UI.GrassLand
{
    public class SettingImpMaxArea : SettingImpBase
    {
        protected override bool UpdateResult(Context ctx)
        {
            try
            {
                IFeatureClass dest = ctx.Dest;
                List<UpdateFeatureInfo> allFeatures = ctx.Features;
                var f_check_index = dest.FindField(SampleSetting.FLD_STATECODE);
                var f_check = dest.Fields.getField(f_check_index);

                VectorDataOperator.StartEdit(dest);
                var fldMj = dest.FindField(SampleSetting.FLD_MJ);
                var listAreas = allFeatures.Select(fea => new { fea.OID, Area = Convert.ToDouble(fea.FeatureBuffer[fldMj].ToString()) });
                foreach (var info in listAreas.OrderByDescending(r => r.Area).Take(settingInfo.SampleCount))
                {
                    var fea = dest.GetFeature(info.OID);
                    fea[f_check_index] = AutoIntValue.IntValue(f_check, SampleSetting.STATE_CODE_SELECTED);
                    fea.Store();
                }
                VectorDataOperator.StopEdit(dest, true);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
                return false;
            }
        }
    }
}
