﻿using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Basic.System;
using Geoway.Data.Geodatabase;
using System;

namespace Geoway.DMS.UI.GrassLand
{
    internal class AutoIntValue
    {
        internal static IDataValue IntValue(IField field, int value)
        {
            if (field.DataType == DataType.Int16)
            {
                return FieldUtil.Convert2GeowayDataValue(DataType.Int16, value);
            }
            else if (field.DataType == DataType.Int32)
            {
                return FieldUtil.Convert2GeowayDataValue(DataType.Int32, value);
            }
            else if (field.DataType == DataType.Int64)
            {
                return FieldUtil.Convert2GeowayDataValue(DataType.Int64, value);
            }

            return null;

        }
        internal static IDataValue IntValue(int fldIndex, IFeatureClass featureClass, int value)
        {
            var fld = featureClass.Fields.getField(fldIndex);
            return IntValue(fld, value);
        }

    }
}