﻿using System.Collections.Generic;

namespace Geoway.DMS.UI.GrassLand.QuadTree
{
    public class Node
    {
        public Node(double x, double y, double width, double height, List<Point> pts)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            Points = pts;
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }

        public List<Point> Points { get; set; } = new List<Point>();
        public List<Node> Children { get; set; } = new List<Node>();

        public override string ToString()
        {
            return $"{X},{Y}";
        }
    }
}
