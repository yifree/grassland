﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Geoway.DMS.UI.GrassLand.QuadTree
{
    public class QuardTreeCreator
    {
        /// <summary>
        /// 叶子节点
        /// </summary>
        public List<Node> Nodes { get; private set; } = new List<Node>();

        public List<Point> Points { get; set; } = new List<Point>();

        public double XMin { get; set; }

        public double XMax { get; set; }

        public double YMin { get; set; }

        public double YMax { get; set; }

        public int MaxNodeCount { get; set; }

        public void SetPoints(List<Point> points)
        {
            Points = points;
            XMax = Points[0].X;
            XMin = Points[0].X;
            YMin = Points[0].Y;
            YMax = Points[0].Y;
            foreach (var pt in points)
            {
                if (pt.X > XMax) XMax = pt.X;
                if (pt.X < XMin) XMin = pt.X;
                if (pt.Y > YMax) YMax = pt.Y;
                if (pt.Y < YMin) YMin = pt.Y;
            }
        }

        public void Create()
        {
            var root = new Node(XMin, YMin, XMax - XMin, YMax - YMin, Points);
            Nodes.Clear();
            Nodes.Add(root);
            recursive_subdivide(root);
        }


        void recursive_subdivide(Node parent)
        {
            if (Nodes.Count >= MaxNodeCount)
                return;

            double w = parent.Width / 2;
            double h = parent.Height / 2;

            var pts = contains(parent.X, parent.Y, w, h, parent.Points);
            if (pts.Count > 0)
            {
                var node = new Node(parent.X, parent.Y, w, h, pts);
                parent.Children.Add(node);
                Nodes.Add(node);
            }

            pts = contains(parent.X, parent.Y + h, w, h, parent.Points);
            if (pts.Count > 0)
            {
                var node = new Node(parent.X, parent.Y + h, w, h, pts);
                parent.Children.Add(node);
                Nodes.Add(node);
            }

            pts = contains(parent.X + w, parent.Y, w, h, parent.Points);
            if (pts.Count > 0)
            {
                var node = new Node(parent.X + w, parent.Y, w, h, pts);
                parent.Children.Add(node);
                Nodes.Add(node);
            }

            pts = contains(parent.X + w, parent.Y + h, w, h, parent.Points);
            if (pts.Count > 0)
            {
                var node = new Node(parent.X + w, parent.Y + h, w, h, pts);
                parent.Children.Add(node);
                Nodes.Add(node);
            }

            if (parent.Children.Count > 0)
            {
                Nodes.Remove(parent);
            }

            while (Nodes.Count < MaxNodeCount)
            {
                var node = Nodes.OrderByDescending(r => r.Points.Count).FirstOrDefault();
                recursive_subdivide(node);
            }
        }

        List<Point> contains(double x, double y, double w, double h, List<Point> points)
        {
            List<Point> nodes = new List<Point>();
            foreach (var point in points)
            {
                if (point.X >= x && point.X <= (x + w) && point.Y > y && point.Y < (y + h))
                {
                    nodes.Add(point);
                }
            }
            return nodes;
        }

        public List<Point> GetCenterPoints()
        {
            List<Point> pts = new List<Point>();
            foreach (var node in Nodes.Take(MaxNodeCount))
            {
                Dictionary<Point, double> dic = new Dictionary<Point, double>();
                var center = new Point((node.X + node.Width / 2), (node.Y + node.Height / 2));
                foreach (var pt in node.Points)
                {
                    dic.Add(pt, Math.Pow(pt.X - center.X, 2) + Math.Pow(pt.Y - center.Y, 2));
                }
                pts.Add(dic.OrderBy(r => r.Value).First().Key);
            }
            return pts;
        }

        public static void Test()
        {
            Form form = new Form() { StartPosition = FormStartPosition.CenterScreen, WindowState = FormWindowState.Maximized };
            PictureBox pic = new PictureBox() { Dock = DockStyle.Fill };
            form.Controls.Add(pic);
            pic.Click += (s, e) => { pic.Invalidate(); };
            pic.Paint += (s, e) =>
             {
                 var pts = new List<Point>();
                 var rnd = new Random();
                 var g = e.Graphics;
                 g.FillRectangle(Brushes.White, e.ClipRectangle);

                 for (int i = 0; i < 100; i++)
                 {
                     var pt = new Point(rnd.Next(200, 700) * 1.0, rnd.Next(200, 700) * 1.0);
                     pts.Add(pt);
                     //g.FillEllipse(Brushes.Green, (int)pt.X, (int)pt.Y, 6, 6);
                 }
                 QuadTree.QuardTreeCreator creator = new QuardTreeCreator();
                 creator.MaxNodeCount = 10;
                 creator.SetPoints(pts);
                 creator.XMin = 100;
                 creator.XMax = 800;
                 creator.YMin = 100;
                 creator.YMax = 800;
                 creator.Create();
                 foreach (var node in creator.Nodes)
                 {
                     //var c = Color.FromArgb(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
                     var c = System.Drawing.Color.Black;
                     g.DrawRectangle(new Pen(c, 8), (int)(node.X + node.Width / 2), (int)(node.Y + node.Height / 2), 4, 4);
                     g.DrawRectangle(new Pen(c, 2), (int)node.X, (int)node.Y, (int)node.Width, (int)node.Height);
                     g.DrawString($"{node.Points.Count}", new Font(FontFamily.Families.First(), 15) { }, Brushes.Blue, (float)node.X, (float)node.Y);
                     foreach (var pt in node.Points)
                     {
                         g.FillEllipse(new SolidBrush(c), (int)pt.X, (int)pt.Y, 8, 8);
                     }
                 }

                 pts = creator.GetCenterPoints();
                 foreach (var pt in pts)
                 {
                     g.FillEllipse(Brushes.Red, (int)pt.X, (int)pt.Y, 10, 10);
                 }
             };

            form.ShowDialog();
        }
    }
}
