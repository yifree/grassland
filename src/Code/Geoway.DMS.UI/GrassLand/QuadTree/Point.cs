﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geoway.DMS.UI.GrassLand.QuadTree
{
    public class Point
    {
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; set; }

        public double Y { get; set; }

        public object Data { get; set; }

        public override string ToString()
        {
            return $"{X},{Y}";
        }
    }
}
