﻿namespace Geoway.DMS.UI.GrassLand
{
    partial class UCGrassResult
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabMain = new DevExpress.XtraTab.XtraTabControl();
            this.pageData = new DevExpress.XtraTab.XtraTabPage();
            this.pageBgCatalog = new DevExpress.XtraTab.XtraTabPage();
            this.pageXZQH = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).BeginInit();
            this.tabMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tabMain);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(754, 573);
            this.splitContainerControl1.SplitterPosition = 262;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tabMain
            // 
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedTabPage = this.pageData;
            this.tabMain.Size = new System.Drawing.Size(262, 573);
            this.tabMain.TabIndex = 0;
            this.tabMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pageData,
            this.pageBgCatalog,
            this.pageXZQH});
            // 
            // pageData
            // 
            this.pageData.Name = "pageData";
            this.pageData.Size = new System.Drawing.Size(256, 544);
            this.pageData.Text = "数据组织";
            // 
            // pageBgCatalog
            // 
            this.pageBgCatalog.Name = "pageBgCatalog";
            this.pageBgCatalog.PageVisible = false;
            this.pageBgCatalog.Size = new System.Drawing.Size(256, 544);
            this.pageBgCatalog.Text = "底图目录";
            // 
            // pageXZQH
            // 
            this.pageXZQH.Name = "pageXZQH";
            this.pageXZQH.Size = new System.Drawing.Size(256, 544);
            this.pageXZQH.Text = "行政区划";
            // 
            // UCGrassResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "UCGrassResult";
            this.Size = new System.Drawing.Size(754, 573);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl tabMain;
        private DevExpress.XtraTab.XtraTabPage pageData;
        private DevExpress.XtraTab.XtraTabPage pageBgCatalog;
        private DevExpress.XtraTab.XtraTabPage pageXZQH;
    }
}
