﻿using System;
using System.Data;
using System.IO;
using System.Text;

namespace ConsoleApp2
{
    class CSVHelper
    {
        /// <summary>
        /// 将datatable中的数据保存到csv中
        /// </summary>
        /// <param name="dt">数据来源</param>
        /// <param name="savaPath">保存的路径</param>
        /// <param name="strName">保存文件的名称</param>
        public static void ExportToSvc(DataTable dt, string savaPath, string strName)
        {
            string strPath = savaPath + "\\" + strName;//保存到指定目录下

            if (File.Exists(strPath))
            {
                File.Delete(strPath);
            }
            //先打印标头
            StringBuilder strColu = new StringBuilder();
            StringBuilder strValue = new StringBuilder();
            int i = 0;
            StreamWriter sw = new StreamWriter(new FileStream(strPath, FileMode.CreateNew), Encoding.GetEncoding("GB2312"));
            try
            {
                for (i = 0; i <= dt.Columns.Count - 1; i++)
                {
                    strColu.Append(dt.Columns[i].ColumnName);
                    strColu.Append(",");
                }
                strColu.Remove(strColu.Length - 1, 1);//移出掉最后一个,字符
                sw.WriteLine(strColu);
                foreach (DataRow dr in dt.Rows)
                {
                    strValue.Remove(0, strValue.Length);//移出
                    for (i = 0; i <= dt.Columns.Count - 1; i++)
                    {
                        strValue.Append(dr[i].ToString());
                        strValue.Append(",");
                    }
                    strValue.Remove(strValue.Length - 1, 1);//移出掉最后一个,字符
                    sw.WriteLine(strValue);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                sw.Close();
            }
            // 完成导出后，打开
        }

        public static DataTable OpenCSVFile(string filepath)
        {
            DataTable mycsvdt = new DataTable();
            string strpath = filepath; //csv文件的路径
            try
            {
                bool blnFlag = true;

                DataColumn mydc;
                DataRow mydr;

                string strline;
                string[] aryline;
                StreamReader mysr = new StreamReader(strpath, System.Text.Encoding.Default);

                while ((strline = mysr.ReadLine()) != null)
                {
                    aryline = strline.Split(new char[] { ',' });
                    //第一行是列的名字，给datatable加上列名,
                    if (blnFlag)
                    {
                        blnFlag = false;
                        foreach (var item in aryline)
                        {
                            if (!mycsvdt.Columns.Contains(item))
                            {
                                mydc = new DataColumn(item);
                                mycsvdt.Columns.Add(mydc);
                            }
                        }
                        continue;
                    }
                    //填充数据并加入到datatable中
                    mydr = mycsvdt.Rows.Add(aryline);
                }
                mysr.Close();
                return mycsvdt;

            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
