﻿using Geoway.ADF.Core.Utility;
using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Data.Geodatabase;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Geoway.DMS.UI.GrassLand
{
    public class SettingImpAvgDistance : SettingImpBase
    {
        protected override bool UpdateResult(Context ctx)
        {

            try
            {
                QuadTree.QuardTreeCreator creator = new QuadTree.QuardTreeCreator();
                creator.MaxNodeCount = settingInfo.SampleCount;
                var pts = ctx.Features.Select(r => new QuadTree.Point(r.Lon, r.Lat) { Data = r.OID });
                creator.SetPoints(pts.ToList());
                creator.Create();
                var centerPts = creator.GetCenterPoints();
                VectorDataOperator.StartEdit(ctx.Dest);
                var f_check_index = ctx.Dest.FindField(SampleSetting.FLD_STATECODE);
                var f_check = ctx.Dest.Fields.getField(f_check_index);
                foreach (var pt in centerPts)
                {
                    var fea = ctx.Dest.GetFeature((int)pt.Data);
                    fea[f_check_index] = AutoIntValue.IntValue(f_check, SampleSetting.STATE_CODE_SELECTED);
                    fea.Store();
                }
                VectorDataOperator.StopEdit(ctx.Dest, true);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
            return false;
        }
    }
}
