﻿using System;

namespace Geoway.DMS.UI.GrassLand
{
    public class SettingImpFactory
    {
        public static SettingImpBase Create(ModelType type)
        {
            switch (type)
            {
                case ModelType.MaxArea:
                    return new SettingImpMaxArea();
                case ModelType.AvgDistance:
                    return new SettingImpAvgDistance();
                default:
                    break;
            }
            throw new NotSupportedException($"不支持类型 {type.ToString()}");
        }
    }
}
