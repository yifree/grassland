﻿namespace Geoway.DMS.UI.GrassLand
{
    partial class UCSchemaSettings
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtSampleCount = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.chkCmbNewType = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.chkCmbType = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.chkCmbNewClass = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.chkCmbClass = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cmbModel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtMinArea = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.btnPath = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.gpLog = new DevExpress.XtraEditors.GroupControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnShowLogInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveResult = new DevExpress.XtraEditors.SimpleButton();
            this.ucxzqh = new Geoway.DMS.UI.GrassLand.UCXZQH();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSampleCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbNewType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbNewClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinArea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(36, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "行政区划：";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.txtSampleCount);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.ucxzqh);
            this.groupControl1.Location = new System.Drawing.Point(3, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1113, 54);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "范围和数量设置";
            // 
            // txtSampleCount
            // 
            this.txtSampleCount.Location = new System.Drawing.Point(577, 25);
            this.txtSampleCount.Name = "txtSampleCount";
            this.txtSampleCount.Size = new System.Drawing.Size(179, 20);
            this.txtSampleCount.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(511, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "样地数量：";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.groupControl3);
            this.groupControl2.Controls.Add(this.cmbModel);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.txtMinArea);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Location = new System.Drawing.Point(3, 59);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1113, 128);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "参数设置";
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.chkCmbNewType);
            this.groupControl3.Controls.Add(this.chkCmbType);
            this.groupControl3.Controls.Add(this.chkCmbNewClass);
            this.groupControl3.Controls.Add(this.chkCmbClass);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Location = new System.Drawing.Point(0, 51);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1113, 103);
            this.groupControl3.TabIndex = 8;
            // 
            // chkCmbNewType
            // 
            this.chkCmbNewType.EditValue = "";
            this.chkCmbNewType.Location = new System.Drawing.Point(577, 51);
            this.chkCmbNewType.Name = "chkCmbNewType";
            this.chkCmbNewType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkCmbNewType.Size = new System.Drawing.Size(179, 20);
            this.chkCmbNewType.TabIndex = 4;
            // 
            // chkCmbType
            // 
            this.chkCmbType.EditValue = "";
            this.chkCmbType.Location = new System.Drawing.Point(577, 25);
            this.chkCmbType.Name = "chkCmbType";
            this.chkCmbType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkCmbType.Size = new System.Drawing.Size(179, 20);
            this.chkCmbType.TabIndex = 4;
            // 
            // chkCmbNewClass
            // 
            this.chkCmbNewClass.EditValue = "";
            this.chkCmbNewClass.Location = new System.Drawing.Point(102, 51);
            this.chkCmbNewClass.Name = "chkCmbNewClass";
            this.chkCmbNewClass.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkCmbNewClass.Size = new System.Drawing.Size(179, 20);
            this.chkCmbNewClass.TabIndex = 4;
            // 
            // chkCmbClass
            // 
            this.chkCmbClass.EditValue = "";
            this.chkCmbClass.Location = new System.Drawing.Point(102, 25);
            this.chkCmbClass.Name = "chkCmbClass";
            this.chkCmbClass.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkCmbClass.Size = new System.Drawing.Size(179, 20);
            this.chkCmbClass.TabIndex = 4;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(511, 54);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 12;
            this.labelControl9.Text = "新草地型：";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(535, 28);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 14);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "型中：";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(36, 54);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 8;
            this.labelControl8.Text = "新草地类：";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(60, 28);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(36, 14);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "类中：";
            // 
            // cmbModel
            // 
            this.cmbModel.Location = new System.Drawing.Point(577, 26);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbModel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbModel.Size = new System.Drawing.Size(179, 20);
            this.cmbModel.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(511, 29);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "筛选模型：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(287, 29);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(12, 14);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "亩";
            // 
            // txtMinArea
            // 
            this.txtMinArea.Location = new System.Drawing.Point(102, 26);
            this.txtMinArea.Name = "txtMinArea";
            this.txtMinArea.Size = new System.Drawing.Size(179, 20);
            this.txtMinArea.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(18, 29);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(78, 14);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "图斑面积>=：";
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.btnPath);
            this.groupControl4.Controls.Add(this.labelControl10);
            this.groupControl4.Location = new System.Drawing.Point(3, 187);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1113, 53);
            this.groupControl4.TabIndex = 9;
            this.groupControl4.Text = "样地成果存储路径";
            // 
            // btnPath
            // 
            this.btnPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPath.Location = new System.Drawing.Point(102, 26);
            this.btnPath.Name = "btnPath";
            this.btnPath.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.btnPath.Properties.Appearance.Options.UseBackColor = true;
            this.btnPath.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnPath.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnPath.Size = new System.Drawing.Size(993, 20);
            this.btnPath.TabIndex = 14;
            this.btnPath.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnPath_ButtonClick);
            this.btnPath.DoubleClick += new System.EventHandler(this.BtnPath_DoubleClick);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(36, 29);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 14);
            this.labelControl10.TabIndex = 13;
            this.labelControl10.Text = "输出路径：";
            // 
            // gpLog
            // 
            this.gpLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpLog.Location = new System.Drawing.Point(3, 239);
            this.gpLog.Name = "gpLog";
            this.gpLog.Size = new System.Drawing.Size(1113, 485);
            this.gpLog.TabIndex = 15;
            this.gpLog.Text = "日志信息";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnShowLogInfo);
            this.panelControl1.Controls.Add(this.btnSaveResult);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 730);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1119, 34);
            this.panelControl1.TabIndex = 16;
            // 
            // btnShowLogInfo
            // 
            this.btnShowLogInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowLogInfo.Location = new System.Drawing.Point(868, 6);
            this.btnShowLogInfo.Name = "btnShowLogInfo";
            this.btnShowLogInfo.Size = new System.Drawing.Size(112, 23);
            this.btnShowLogInfo.TabIndex = 1;
            this.btnShowLogInfo.Text = "统计样地信息";
            this.btnShowLogInfo.Click += new System.EventHandler(this.ShowLogInfo_Click);
            // 
            // btnSaveResult
            // 
            this.btnSaveResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveResult.Location = new System.Drawing.Point(986, 6);
            this.btnSaveResult.Name = "btnSaveResult";
            this.btnSaveResult.Size = new System.Drawing.Size(112, 23);
            this.btnSaveResult.TabIndex = 0;
            this.btnSaveResult.Text = "输出样地成果";
            this.btnSaveResult.Click += new System.EventHandler(this.BtnSaveResult_Click);
            // 
            // ucxzqh
            // 
            this.ucxzqh.Location = new System.Drawing.Point(102, 28);
            this.ucxzqh.Name = "ucxzqh";
            this.ucxzqh.Size = new System.Drawing.Size(360, 25);
            this.ucxzqh.TabIndex = 0;
            // 
            // UCSchemaSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gpLog);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "UCSchemaSettings";
            this.Size = new System.Drawing.Size(1119, 764);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSampleCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbNewType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbNewClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCmbClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinArea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UCXZQH ucxzqh;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtSampleCount;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtMinArea;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit cmbModel;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkCmbNewType;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkCmbType;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkCmbNewClass;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkCmbClass;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.ButtonEdit btnPath;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.GroupControl gpLog;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSaveResult;
        private DevExpress.XtraEditors.SimpleButton btnShowLogInfo;
    }
}
