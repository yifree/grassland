﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geoway.DMS.UI.GrassLand
{
    static class StringUtil
    {
        //101000==>1010
        public static string getDistCodePart(string code)
        {
            while (code.EndsWith("00"))
            {
                code = code.Substring(0, code.Length - 2);
            }
            return code;
        }
    }
}
