﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Geoway.ADF.Core.Utility;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Geoway.DMS.UI.GrassLand
{
    class ExcelExporter
    {
        public string TempelatePath { get; set; } = "样地成果输出模板.xls";

        public bool Export(DataTable dataTable, string outPath)
        {
            try
            {
                if (!File.Exists(TempelatePath))
                {
                    LogHelper.Error.Append($"未找到模板 {TempelatePath}");
                    return false;
                }

                using (FileStream fsIn = new FileStream(TempelatePath, FileMode.Open, FileAccess.Read))
                {
                    IWorkbook workbook = null;  //新建IWorkbook对象
                    var ext = Path.GetExtension(outPath).ToLower();
                    if (ext == ".xlsx") // 2007版本
                    {
                        workbook = new XSSFWorkbook(fsIn);  //xlsx数据读入workbook
                    }
                    else if (ext == ".xls") // 2003版本
                    {
                        workbook = new HSSFWorkbook(fsIn);  //xls数据读入workbook
                    }
                    else
                        throw new NotSupportedException();
                    var sheet = workbook.GetSheetAt(0);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        var dtRow = dataTable.Rows[i];
                        var row = sheet.CreateRow(i + 1);
                        for (int j = 0; j < dataTable.Columns.Count; j++)
                        {
                            var cell = row.CreateCell(j);
                            cell.SetCellValue(dtRow[j].ToString());
                        }
                    }
                    using (var fsOut = File.OpenWrite(outPath))
                    {
                        workbook.Write(fsOut);//向打开的这个xls文件中写入数据  
                        fsOut.Close();
                    }
                    fsIn.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
                return false;
            }
        }
    }
}
