﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Geoway.ADF.GDC.Catalog;
using Geoway.ADF.GDC.CatalogUI;
using Geoway.ADF.Core.DB;

namespace Geoway.DMS.UI.GrassLand
{
    partial class UCXZQH : XtraUserControl
    {
        public UCDistrictTree DistrictTree { get; private set; }

        public List<GwDistrictItem> Districts
        {
            get
            {
                var item = this.popupContainerEdit1.EditValue as GwDistrictItem;
                if (item == null)
                    return new List<GwDistrictItem>();
                return new List<GwDistrictItem>() { item };
            }
        }

        public UCXZQH()
        {
            InitializeComponent();
            DistrictTree = new UCDistrictTree() { Dock = DockStyle.Fill };
            DistrictTree.MultiSelect = false;
            DistrictTree.Event_ItemDoubleClick += DistrictTree_Event_ItemDoubleClick;
            popupContainerControl1.Controls.Add(DistrictTree);
        }

        private void DistrictTree_Event_ItemDoubleClick(object sender, DistrictTreeItemDoubleClickEventArgs e)
        {
            this.popupContainerEdit1.EditValue = e.DistrictItem;
            popupContainerEdit1.ClosePopup();
        }

        public void Init(IDBHelper db)
        {
            DistrictTree.Initialize(db);
            var userCatalogID = UserCatalogConfig.GetUserCatalogID(db, ADF.MIS.Business.LoginControl.Instance.UserID, EnumGwCatalogUsage.District);
#if DEBUG
            userCatalogID = UserCatalogConfig.GetUserCatalogID(db, 1001, EnumGwCatalogUsage.District);
#endif
            if (userCatalogID != null && userCatalogID.Count > 0)
            {
                DistrictTree.LoadDistrict(userCatalogID.First().ToString());
            }
            DistrictTree.AutoCheckChildNode = false;
        }
    }
}
