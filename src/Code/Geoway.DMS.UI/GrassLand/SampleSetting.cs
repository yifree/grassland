﻿using System;
using System.Collections.Generic;
using System.Linq;
using Geoway.ADF.Core.Utility;
using Geoway.ADF.GDC.Catalog;
using Geoway.ADF.GIS.GeowayUtility;
using Geoway.Data.Geodatabase;

namespace Geoway.DMS.UI.GrassLand
{
    /// <summary>
    /// 样地设置信息
    /// </summary>
    public class SampleSetting
    {
        const double MU_2_METERS = 666.666666667;

        public readonly static string TB_DLTB = ConfigUtil.GetConfigValue("TB_DLTB", "dltb");

        public readonly static string TB_GRASS = ConfigUtil.GetConfigValue("TB_NYB_DLTB", "tb_grass");

        public const string FLD_MJ = "tbmj";

        public const string FLD_XZQ = "zldwdm";

        //public const string FLD_BSM = "bsm";//地类标识码

        public const string FLD_CLASS = "ny_lz";//类中

        public const string FLD_CLASS_CODE = FLD_CLASS;

        public const string FLD_TYPE = "ny_xz";//型中

        public const string FLD_TYPE_CODE = FLD_TYPE;

        public const string FLD_NEWCLASS = "ny_xcdl";//新草地类

        public const string FLD_NEWCLASS_CODE = FLD_NEWCLASS;

        public const string FLD_NEWTYPE = "ny_xcdx";//新草地型

        public const string FLD_NEWTYPE_CODE = FLD_NEWTYPE;

        public const string FLD_JCDBH = "ny_jcdbh";//监测点编号

        public const string FLD_YEAR = "nf";//年份

        public const string FLD_DYBM = "dybm";//年份

        public const string FLD_LON = "jd";//经度

        public const string FLD_LAT = "wd";//纬度

        public const string FLD_STATECODE = "f_checked";

        public const string FLD_DLBM = "dlbm";

        //取消选择
        public const int STATE_CODE_UNSELECTED = 0;
        //符合条件样地前N个
        public const int STATE_CODE_SELECTED = 1;
        //其他符合条件样地
        public const int STATE_CODE_OPTIONAL = 2;
        //仅符合面积样地
        public const int STATE_CODE_AREA_OK = 3;

        public ModelType ModelType { get; set; } = ModelType.MaxArea;

        public string Name { get; set; }

        public string Path { get; set; }

        public string[] OldClasses { get; set; }

        public string[] OldTypes { get; set; }

        public string[] NewClasses { get; set; }

        public string[] NewTypes { get; set; }

        public int SampleCount { get; set; }

        public DateTime CreatedAt { get; set; }

        //原则上只有一个
        public List<GwDistrictItem> DistrictList { get; set; }

        public double MinArea { get; internal set; }

        public double MinAreaMeters
        {
            get
            {
                return Math.Round(MinArea * MU_2_METERS, 4);
            }
        }

        public string GetOutputName()
        {
            string outputName = $"{string.Join("-", DistrictList.Take(5))}{ModelType2Name(ModelType)}样地成果({SampleCount}){DateTime.Now.ToString("yyyyMMdd")}";
            return outputName;

        }

        public static string ModelType2Name(ModelType type)
        {
            switch (type)
            {
                case ModelType.MaxArea:
                    return "最大面积法";
                case ModelType.AvgDistance:
                    return "平均距离法";
                default:
                    break;
            }
            return "";
        }

    }
}
