﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Geoway.ADF.GIS.GeowayControls;
using Geoway.Data.Geodatabase;
using Geoway.ADF.Core.Utility;
using Geoway.ADF.Core.Framework;
using Geoway.ADF.Core.DB;
using Geoway.Logic.Carto;
using Geoway.ADF.GIS.GeowayCatalog;
using Geoway.ADF.GIS.GeowayUtility;
using System.Collections.Generic;
using Geoway.Basic.System;
using System.Data;
using Geoway.ADF.GDC.DataModel;
using System.IO;
using System.Drawing;
using Geoway.ADF.GDC.CatalogUI;
using Geoway.ADF.GIS.GeowayCatalogUI;

namespace Geoway.DMS.UI.GrassLand
{
    partial class UCMap : XtraUserControl
    {
        private IFeatureWorkspace _editingWorkspace;
        private IFeatureLayer _editingLayer;
        private IDBHelper _db;
        private IDBHelper _dbSpatial;
        private bool attrTableOnlyShowSelection = false;
        private readonly Stack<List<FeatureState>> UndoStack = new Stack<List<FeatureState>>();
        private readonly Stack<List<FeatureState>> RedoStack = new Stack<List<FeatureState>>();
        private readonly Dictionary<string, Color> CHECK_FIELD_COLOR_MAPPING = new Dictionary<string, Color>();
        private readonly Dictionary<IGwCatalogNode, bool> loadedLayers = new Dictionary<IGwCatalogNode, bool>();
        private readonly GwDataNodeAttTableDialog gwDataNodeAttTableDialog = new GwDataNodeAttTableDialog();
        private readonly IGeowayMapControl geowayMapControl;

        const string TEXT_STOP = "停止";
        const string TEXT_START = "调整";

        public IGwMapPreview MapPreview { get; }
        public UCGwCatalogControl MainCatalog { get; internal set; }

        public UCMap()
        {
            InitializeComponent();
            MapPreview = new UCMapPreview() { Dock = DockStyle.Fill, ToolBarDockStyle = DevExpress.XtraBars.BarDockStyle.Right };
            geowayMapControl = MapPreview.MapControl as IGeowayMapControl;
            this.Controls.Add(MapPreview as Control);
            Dock = DockStyle.Fill;
            btnUndo.Enabled = false;
            SetButtonStatus(false);
            repositoryItemComboBox1.EditValueChanging += EditLayer_Changing;
            Load += UCMap_Load;
        }

        private void UCMap_Load(object sender, EventArgs e)
        {
            MapPreview.MapControl.FullExtent();
        }

        public void Init(IDBHelper db, IDBHelper dbSpatial)
        {
            _db = db;
            _dbSpatial = dbSpatial;

            initColors();
        }

        internal void OnLayerLoaded(object sender, CatalogDataNodeEventArgs e)
        {
            if (loadedLayers.ContainsKey(e.CatalogDataNode) && !loadedLayers[e.CatalogDataNode])
            {
                var gwLayer = e.CatalogDataNode as IGwGeowayFeatureLayer;
                var lyr = gwLayer.GeowayLayer as IFeatureLayer;
                if (lyr != null)
                {
                    RenderUtilEx.UniqueValueRender(lyr, SampleSetting.FLD_STATECODE, gwLayer.GeowayLayerState, CHECK_FIELD_COLOR_MAPPING);
                    loadedLayers[e.CatalogDataNode] = true;
                }
                this.MapPreview.MapControl.ZoomToLayer(gwLayer);
                //var env = lyr.FeatureClass.Extent;
                //this.MapPreview.MapControl.ZoomToExtent(env.XMin, env.XMax, env.YMin, env.YMax);

            }
        }

        private void initColors()
        {
            CHECK_FIELD_COLOR_MAPPING.Add(SampleSetting.STATE_CODE_UNSELECTED.ToString(), string2Color(ConfigUtil.GetConfigValue("checked-color-0", "yellow")));
            CHECK_FIELD_COLOR_MAPPING.Add(SampleSetting.STATE_CODE_SELECTED.ToString(), string2Color(ConfigUtil.GetConfigValue("checked-color-1", "red")));
            CHECK_FIELD_COLOR_MAPPING.Add(SampleSetting.STATE_CODE_OPTIONAL.ToString(), string2Color(ConfigUtil.GetConfigValue("checked-color-2", "yellow")));
            CHECK_FIELD_COLOR_MAPPING.Add(SampleSetting.STATE_CODE_AREA_OK.ToString(), string2Color(ConfigUtil.GetConfigValue("checked-color-3", "yellow")));
        }

        private Color string2Color(string colorText)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(colorText))
                    throw new Exception("颜色值为空");
                ColorConverter colorConverter = new ColorConverter();
                return (Color)colorConverter.ConvertFromString(colorText);
            }
            catch (Exception ex)
            {
                long tick = DateTime.Now.Ticks;
                Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));

                int R = ran.Next(255);
                int G = ran.Next(255);
                int B = ran.Next(255);
                B = (R + G > 400) ? R + G - 400 : B;//0 : 380 - R - G;
                B = (B > 255) ? 255 : B;
                return Color.FromArgb(R, G, B);
            }
        }

        private void EditLayer_Changing(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (_editingWorkspace != null && _editingWorkspace.Workspace.IsInEditing)
            {
                DevMsg.YesNo("有未保存的编辑，请先停止编辑");
                e.Cancel = true;
                return;
            }

            var gwLayer = e.NewValue as IGwGeowayFeatureLayer;
            var lyr = gwLayer.GeowayLayer as IFeatureLayer;
            if (lyr == null)
            {
                DevMsg.Error("请先打开图层");
                e.Cancel = true;
                return;
            }

            SetEditLayer(gwLayer, lyr);
        }

        internal void LayerAdded(IGwCatalogNode curNode)
        {
            var dataset = (curNode as IGwCatalogDataNode).DataSet as ADF.GDC.Dataset.GwSpatialTable;
            if (dataset != null)
            {
                if (!dataset.Fields.Exists(r => r.Name.Equals(SampleSetting.FLD_JCDBH, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return;
                }

                if (!dataset.Fields.Exists(r => r.Name.Equals(SampleSetting.FLD_STATECODE, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return;
                }
            }
            this.loadedLayers.Add(curNode, false);
            repositoryItemComboBox1.Items.Add(curNode);
        }


        void SetEditLayer(IGwGeowayFeatureLayer gwLayer, IFeatureLayer layer)
        {
            gwLayer.CanSelect = true;
            _editingWorkspace = layer.FeatureClass.Workspace as IFeatureWorkspace;
            _editingLayer = layer;
        }

        void SetButtonStatus(bool editing)
        {
            btnStartEdit.Caption = editing ? TEXT_STOP : TEXT_START;
            btnStartEdit.Enabled = true;
            btnSave.Enabled = editing;
            btnExport.Enabled = !editing;
            btnUpload.Enabled = !editing;
            btnSelect.Enabled = editing;
            btnUnSelect.Enabled = editing;
        }

        private void BtnStartEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (_editingLayer == null)
                {
                    DevMsg.Error("未选择编辑图层");
                    return;
                }

                if (btnStartEdit.Caption == TEXT_STOP)
                {
                    if (UndoStack.Count > 0)
                    {
                        if (DevMsg.YesNo("有未保存的编辑，是否保存？") == DialogResult.Yes)
                        {
                            _editingWorkspace.StopEditing(true, true);
                        }
                        else
                        {
                            _editingWorkspace.StopEditing(false, true);
                        }
                    }
                    else
                    {
                        _editingWorkspace.StopEditing(false, true);
                    }
                    SetButtonStatus(false);
                    this.gwDataNodeAttTableDialog.Visible = false;
                    return;
                }
                else
                {
                    VectorDataOperator.StartEdit(_editingLayer.FeatureClass);
                    var tool = new EnvelopeSelectTool();
                    tool.Context = geowayMapControl.MapControl.Context;
                    geowayMapControl.MapControl.ActiveTool = tool;
                    tool.Execute();
                    SetButtonStatus(true);

                    this.gwDataNodeAttTableDialog.Initialize(this._editingLayer, attrTableOnlyShowSelection);
                    this.gwDataNodeAttTableDialog.Owner = this.FindForm();
                    this.gwDataNodeAttTableDialog.Show();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
        }

        private void BtnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                _editingWorkspace.UndoEditOperation();
                var states = UndoStack.Pop();
                var fldIndex = _editingLayer.FeatureClass.FindField(SampleSetting.FLD_STATECODE);
                foreach (var state in states)
                {
                    var fea = _editingLayer.FeatureClass.GetFeature(state.OID);
                    fea[fldIndex] = state.State;
                    fea.Store();
                }
                btnUndo.Enabled = UndoStack.Count > 0;
                MapPreview.MapControl.RefreshMap();
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
        }

        private void BtnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                _editingWorkspace.StopEditing(true, false);
                UndoStack.Clear();
                btnUndo.Enabled = UndoStack.Count > 0;
                DevMsg.Info("保存成功");
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
                DevMsg.Error("保存失败");
            }
        }

        private void BtnExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (_editingLayer == null)
            {
                DevMsg.Error("请先选择当前样地成果");
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel(工作簿)|*.xls";
            if (saveFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            var pCur = _editingLayer.FeatureClass.Search(new LiteralConditionClass() { WhereClause = $"{SampleSetting.FLD_STATECODE}=1" }, false);
            if (pCur == null)
            {
                DevMsg.Error("未查询到图斑");
                return;
            }

            var dt = new DataTable();
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "年份" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "监测点编号" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "地域编码" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "经度" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "纬度" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "类中" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "型中" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "新草地类" });
            dt.Columns.Add(new DataColumn() { DataType = typeof(string), ColumnName = "新草地型" });

            IFeature fea;
            int count = 0;
            while ((fea = pCur.NextFeature()) != null)
            {
                count++;
                dt.Rows.Add(new object[] {
                    DateTime.Now.Year,//年份
                    fea.GetValue(pCur.FindField(SampleSetting.FLD_JCDBH)),//监测点编号：由12位数字组成，前6位为地域编码，后6位为图斑顺序号
                    fea.GetValue(pCur.FindField(SampleSetting.FLD_DYBM)),//地域编码
                   Convert.ToDouble( fea.GetValue(pCur.FindField(SampleSetting.FLD_LON)).ToString()).ToString("#.######"),
                   Convert.ToDouble( fea.GetValue(pCur.FindField(SampleSetting.FLD_LAT)).ToString()).ToString("#.######"),
                    fea.GetValue(_editingLayer.FeatureClass.FindField(SampleSetting.FLD_CLASS)),
                    fea.GetValue(pCur.FindField(SampleSetting.FLD_TYPE)),
                    fea.GetValue(pCur.FindField(SampleSetting.FLD_NEWCLASS)),
                    fea.GetValue(pCur.FindField(SampleSetting.FLD_NEWTYPE)),
                });
            }
            ExcelExporter exporter = new ExcelExporter();
            var ok = exporter.Export(dt, saveFileDialog.FileName);
            if (ok)
            {
                DevMsg.Info("导出成功");
            }
            else
            {
                DevMsg.Error("导出失败");
            }
        }

        private void BtnUpload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (_editingLayer == null)
                {
                    DevMsg.Error("请先选择当前样地成果");
                    return;
                }

                string name = "样地共享存储";
                var id = _db.QueryScalar($"select f_id from tbcm_servers where f_name='{name}'")?.ToString();
                if (id == null)
                {
                    DevMsg.Error($"未找到名称为 {name} 的存储配置");
                    return;
                }
                string folder = _editingLayer.FeatureClass.Workspace.ConnectionString;
                var serverParam = CatalogModelEngine.GetStorageNodeByID(_db, Convert.ToInt32(id));
                if (serverParam != null)
                {
                    var server = CatalogModelEngine.CreateCatalogDataSource(serverParam);
                    if (server != null)
                    {
                        var files = System.IO.Directory.GetFiles(folder, _editingLayer.FeatureClass.Name + ".*");
                        foreach (var file in files)
                        {
                            server.PutSingleFile(file, Path.GetFileName(file));
                        }
                        DevMsg.Info("上传成功");
                    }
                    else
                    {
                        DevMsg.Error("创建存储对象失败");
                    }
                }
            }
            catch (Exception ex)
            {
                _dbSpatial.Rollback();
                DevMsg.Error("上传失败：" + ex.Message);
                LogHelper.Error.Append(ex);
            }
        }

        internal void LayerRemoved(IGwCatalogNode deleteNode)
        {

        }

        private void BtnSelect_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                var pCur = _editingLayer.SelectionSet.Search(null, false);
                if (pCur == null)
                    return;
                var fldIndex = _editingLayer.FeatureClass.FindField(SampleSetting.FLD_STATECODE);
                var fld = _editingLayer.FeatureClass.Fields.getField(fldIndex);
                var list = new List<FeatureState>();
                IRow feature;
                while ((feature = pCur.NextRow()) != null)
                {
                    list.Add(new FeatureState(feature.OID, feature[fldIndex]));
                    var val = AutoIntValue.IntValue(fld, SampleSetting.STATE_CODE_SELECTED);
                    feature[fldIndex] = val;
                    feature.Store();
                }
                UndoStack.Push(list);
                _editingLayer.SelectionSet.Clear();
                btnUndo.Enabled = UndoStack.Count > 0;
                (this.MapPreview.MapControl).RefreshMap();
            }
            catch (Exception ex)
            {
                LogHelper.Error.Append(ex);
            }
        }

        private void BtnUnSelect_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                var pCur = _editingLayer.SelectionSet.Search(null, false);
                if (pCur == null)
                    return;
                var fldIndex = _editingLayer.FeatureClass.FindField(SampleSetting.FLD_STATECODE);
                var field = _editingLayer.FeatureClass.Fields.getField(fldIndex);
                var list = new List<FeatureState>();
                IRow feature;
                while ((feature = pCur.NextRow()) != null)
                {
                    list.Add(new FeatureState(feature.OID, feature[fldIndex]));
                    var val = AutoIntValue.IntValue(field, SampleSetting.STATE_CODE_UNSELECTED);
                    feature[fldIndex] = val;
                    feature.Store();
                }
                _editingLayer.SelectionSet.Clear();
                UndoStack.Push(list);
                btnUndo.Enabled = UndoStack.Count > 0;
                (this.MapPreview.MapControl).RefreshMap();
            }
            catch (Exception ex)
            {
                _editingWorkspace.AbortEditOperation();
                LogHelper.Error.Append(ex);
            }
        }

        private void BtnAttrTable_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.gwDataNodeAttTableDialog.Visible)
                return;
            if (_editingLayer == null)
            {
                DevMsg.Error("未选择编辑图层");
                return;
            }
            this.gwDataNodeAttTableDialog.Initialize(this._editingLayer, attrTableOnlyShowSelection);
            this.gwDataNodeAttTableDialog.Owner = this.FindForm();
            this.gwDataNodeAttTableDialog.Show();
        }

        class FeatureState
        {
            public FeatureState(int oid, IDataValue state)
            {
                OID = oid;
                State = state;
            }

            public int OID { get; set; }

            public IDataValue State { get; set; }
        }
    }
}
