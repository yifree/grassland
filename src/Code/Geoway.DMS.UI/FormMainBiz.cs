﻿using DevExpress.XtraBars;
using Geoway.DMS.UI.GrassLand;
using Geoway.GDC.DMS.UI;

namespace Geoway.DMS.UI
{
    /// <summary>
    /// 主窗体
    /// </summary>
    public partial class FormMainBiz : FormMain
    {
        private UIGeneratorBiz generator;

        /// <summary>
        /// 构造
        /// </summary>
        public FormMainBiz() : base()
        {
        }

        protected override void InitGenerator()
        {
            generator = new UIGeneratorBiz(_dbHelper, ribbon, pnlMain);
            generator.SetSplashBackMsg(_callSplashMsg);
            generator.GetMainFormEvent += _generator_GetMainFormEvent;
            generator.ShowOperationStatus += _generator_ShowOperationStatus;
            base._generator = generator;
        }


        protected override void InitDataProcess()
        {
            base.InitDataProcess();

            var group = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            var btnGrassSchema = new BarButtonItem()
            {
                Caption = "样地参数设置",
                PaintStyle = BarItemPaintStyle.CaptionGlyph
            };
            btnGrassSchema.LargeGlyph = ImageUtil.LoadImage(btnGrassSchema.Caption);
            generator.InitGrassSchema(btnGrassSchema);
            group.ItemLinks.Add(btnGrassSchema);

            var btnGrassResult = new BarButtonItem()
            {
                Caption = "样地成果调整",
                PaintStyle = BarItemPaintStyle.CaptionGlyph
            };
            btnGrassResult.LargeGlyph = ImageUtil.LoadImage(btnGrassResult.Caption);
            generator.InitGrassResult(btnGrassResult);
            group.ItemLinks.Add(btnGrassResult);

            ribbonPagePre.Groups.Add(group);
        }
    }
}
