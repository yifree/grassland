msbuild /t:rebuild -p:Configuration=Release /verbosity:minimal /consoleloggerparameters:ErrorsOnly Geoway.MapStore.sln
set dist=.\dist\
set outDir=.\Output\Release\
copy %outDir%Geoway.DMS.Tools.GrassMerge.App.exe*  %dist%
copy %outDir%Geoway.DMS.UI.dll  %dist%
copy %outDir%使用说明.txt  %dist%
copy %outDir%grass_merge_fields.txt  %dist%
copy %outDir%综合数据库管理平台.exe*  %dist%
pause